LU method  |  Description                                                 
--------------------------------------------------------------------------
default    |  default LU solver                                           
umfpack    |  UMFPACK (Unsymmetric MultiFrontal sparse LU factorization)  
mumps      |  MUMPS (MUltifrontal Massively Parallel Sparse direct Solver)
petsc      |  PETSc builtin LU solver                                     

###################################
Number of mesh refinements: 0
###################################
Number of mesh elements: 15576
Number of mesh edges: 23444
Number of mesh nodes: 7869

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
--------------------------------------------------- 

Solving coupled Darcy-Stokes system of size <62464 x 62464>
Integral flux: 1.24537e-11
Elapsed time: 3.21262 (Solving 0th refinement step)
h_min: 1.96576e-06

###################################
Number of mesh refinements: 1
###################################
Number of mesh elements: 62304
Number of mesh edges: 93616
Number of mesh nodes: 31313

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
--------------------------------------------------- 

Solving coupled Darcy-Stokes system of size <249536 x 249536>
Integral flux: 1.24529e-11
Elapsed time: 12.2792 (Solving 1st refinement step)
h_min: 9.82882e-07
L2-Norm velocity: 1.90556e-08
L2-Norm pressure: 0.00224303

###################################
Number of mesh refinements: 2
###################################
Number of mesh elements: 249216
Number of mesh edges: 374144
Number of mesh nodes: 124929

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
--------------------------------------------------- 

Solving coupled Darcy-Stokes system of size <997504 x 997504>
Integral flux: 1.24528e-11
Elapsed time: 54.2159 (Solving 2nd refinement step)
h_min: 4.91441e-07
L2-Norm velocity: 5.18205e-09
L2-Norm pressure: 0.00112164
Experimental order of convergence (velocity): 1.87862
Experimental order of convergence (pressure): 0.999841

###################################
Number of mesh refinements: 3
###################################
Number of mesh elements: 996864
Number of mesh edges: 1495936
Number of mesh nodes: 499073

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
--------------------------------------------------- 

Solving coupled Darcy-Stokes system of size <3988736 x 3988736>
Integral flux: 1.24528e-11
Elapsed time: 570.193 (Solving 3rd refinement step)
h_min: 2.4572e-07
L2-Norm velocity: 1.36015e-09
L2-Norm pressure: 0.000560859
Experimental order of convergence (velocity): 1.92976
Experimental order of convergence (pressure): 0.9999
