LU method  |  Description                                                 
--------------------------------------------------------------------------
default    |  default LU solver                                           
umfpack    |  UMFPACK (Unsymmetric MultiFrontal sparse LU factorization)  
mumps      |  MUMPS (MUltifrontal Massively Parallel Sparse direct Solver)
petsc      |  PETSc builtin LU solver                                     

###################################
Number of mesh refinements: 0
###################################
Number of mesh elements: 15576
Number of mesh edges: 23444
Number of mesh nodes: 7869

Parameters
--------------------------------------------------- 
mu_blood = 0.0035
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
--------------------------------------------------- 

Solving coupled Darcy-Stokes system of size <62464 x 62464>
Integral flux: 1.24497e-11
Elapsed time: 3.171 (Solving 0th refinement step)

###################################
Number of mesh refinements: 1
###################################
Number of mesh elements: 62304
Number of mesh edges: 93616
Number of mesh nodes: 31313

Parameters
--------------------------------------------------- 
mu_blood = 0.0035
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
--------------------------------------------------- 

Solving coupled Darcy-Stokes system of size <249536 x 249536>
Integral flux: 1.24511e-11
Elapsed time: 12.0048 (Solving 1st refinement step)

###################################
Number of mesh refinements: 2
###################################
Number of mesh elements: 249216
Number of mesh edges: 374144
Number of mesh nodes: 124929
