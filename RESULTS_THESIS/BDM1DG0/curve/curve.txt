LU method  |  Description                                                 
--------------------------------------------------------------------------
default    |  default LU solver                                           
umfpack    |  UMFPACK (Unsymmetric MultiFrontal sparse LU factorization)  
mumps      |  MUMPS (MUltifrontal Massively Parallel Sparse direct Solver)
petsc      |  PETSc builtin LU solver                                     

###################################
Number of mesh refinements: 0
###################################

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
--------------------------------------------------- 

Solving coupled Darcy-Stokes system of size <55253 x 55253>
Integral flux: 1.23978e-11
Elapsed time: 2.87262 (Solving 0th refinement step)

###################################
Number of mesh refinements: 1
###################################

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
--------------------------------------------------- 

Solving coupled Darcy-Stokes system of size <220690 x 220690>
Integral flux: 1.24657e-11
Elapsed time: 10.8436 (Solving 1st refinement step)

###################################
Number of mesh refinements: 2
###################################
error: exterior boundaries of freeflow must be inlet or outlet
error: exterior boundaries of freeflow must be inlet or outlet
error: exterior boundaries of freeflow must be inlet or outlet
error: exterior boundaries of freeflow must be inlet or outlet
