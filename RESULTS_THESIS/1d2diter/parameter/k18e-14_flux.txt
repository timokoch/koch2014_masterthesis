LU method  |  Description                                                 
--------------------------------------------------------------------------
default    |  default LU solver                                           
umfpack    |  UMFPACK (Unsymmetric MultiFrontal sparse LU factorization)  
mumps      |  MUMPS (MUltifrontal Massively Parallel Sparse direct Solver)
petsc      |  PETSc builtin LU solver                                     

###################################
Number of mesh refinements: 0
###################################
Number of mesh elements (ff): 100
Number of mesh edges (ff): 100
Number of mesh nodes (ff): 101
Number of mesh elements (pm): 2000
Number of mesh edges (pm): 3060
Number of mesh nodes (pm): 1061

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 200

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 1
       K = 1.8e-14
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
   gamma = 2
  radius = 4.3e-06
   theta = 0.3
--------------------------------------------------- 


Solving linear Darcy system of size <1061 x 1061>
Solving linear Stokes system of size <101 x 101>
Overall degrees of freedom <1162>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=5000

Iteration 1 done with error = 2084.89
Elapsed time: 0.035248 (iteration step)
Iteration 2 done with error = 387.868
Elapsed time: 0.0259879 (iteration step)
Iteration 3 done with error = 72.2458
Elapsed time: 0.0212352 (iteration step)
Iteration 4 done with error = 13.4798
Elapsed time: 0.02284 (iteration step)
Iteration 5 done with error = 2.52136
Elapsed time: 0.0461001 (iteration step)
Iteration 6 done with error = 0.473339
Elapsed time: 0.0380831 (iteration step)
Iteration 7 done with error = 0.0893483
Elapsed time: 0.0425761 (iteration step)
Iteration 8 done with error = 0.0170052
Elapsed time: 0.028214 (iteration step)
Iteration 9 done with error = 0.00327713
Elapsed time: 0.019598 (iteration step)
Iteration 10 done with error = 0.000643501
Elapsed time: 0.0204668 (iteration step)
Iteration 11 done with error = 0.000129909
Elapsed time: 0.0308211 (iteration step)
Iteration 12 done with error = 2.72785e-05
Elapsed time: 0.021225 (iteration step)
Iteration 13 done with error = 6.03371e-06
Elapsed time: 0.022743 (iteration step)
Iteration 14 done with error = 1.41915e-06
Elapsed time: 0.0300262 (iteration step)
Iteration 15 done with error = 3.55451e-07
Elapsed time: 0.0199521 (iteration step)
Iteration 16 done with error = 9.40853e-08
Elapsed time: 0.0205309 (iteration step)
Iteration 17 done with error = 2.59836e-08
Elapsed time: 0.020983 (iteration step)
Iteration 18 done with error = 7.39268e-09
Elapsed time: 0.0208759 (iteration step)
Iteration 19 done with error = 2.14108e-09
Elapsed time: 0.0218432 (iteration step)
Iteration 20 done with error = 6.28697e-10
Elapsed time: 0.0226941 (iteration step)
Iteration 21 done with error = 1.85625e-10
Elapsed time: 0.0214269 (iteration step)
Iteration 22 done with error = 5.3802e-11
Elapsed time: 0.0210972 (iteration step)
Source integral: 1.71189e-11
Elapsed time: 0.813054 (Solving 0th refinement step)

###################################
Number of mesh refinements: 1
###################################
Number of mesh elements (ff): 200
Number of mesh edges (ff): 200
Number of mesh nodes (ff): 201
Number of mesh elements (pm): 8000
Number of mesh edges (pm): 12120
Number of mesh nodes (pm): 4121

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 400

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 1
       K = 1.8e-14
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
   gamma = 2
  radius = 4.3e-06
   theta = 0.3
--------------------------------------------------- 


Solving linear Darcy system of size <4121 x 4121>
Solving linear Stokes system of size <201 x 201>
Overall degrees of freedom <4322>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=5000

Iteration 1 done with error = 4139.84
Elapsed time: 0.096509 (iteration step)
Iteration 2 done with error = 770.004
Elapsed time: 0.0620499 (iteration step)
Iteration 3 done with error = 143.388
Elapsed time: 0.0622799 (iteration step)
Iteration 4 done with error = 26.7456
Elapsed time: 0.0610769 (iteration step)
Iteration 5 done with error = 5.00077
Elapsed time: 0.0651329 (iteration step)
Iteration 6 done with error = 0.938363
Elapsed time: 0.061913 (iteration step)
Iteration 7 done with error = 0.177023
Elapsed time: 0.0649421 (iteration step)
Iteration 8 done with error = 0.0336676
Elapsed time: 0.0638142 (iteration step)
Iteration 9 done with error = 0.0064825
Elapsed time: 0.0692079 (iteration step)
Iteration 10 done with error = 0.00127163
Elapsed time: 0.06199 (iteration step)
Iteration 11 done with error = 0.000256444
Elapsed time: 0.0614719 (iteration step)
Iteration 12 done with error = 5.38016e-05
Elapsed time: 0.060179 (iteration step)
Iteration 13 done with error = 1.18958e-05
Elapsed time: 0.0621531 (iteration step)
Iteration 14 done with error = 2.79898e-06
Elapsed time: 0.060183 (iteration step)
Iteration 15 done with error = 7.01823e-07
Elapsed time: 0.0625839 (iteration step)
Iteration 16 done with error = 1.8605e-07
Elapsed time: 0.0610142 (iteration step)
Iteration 17 done with error = 5.14713e-08
Elapsed time: 0.0593441 (iteration step)
Iteration 18 done with error = 1.46642e-08
Elapsed time: 0.0589979 (iteration step)
Iteration 19 done with error = 4.25917e-09
Elapsed time: 0.060046 (iteration step)
Iteration 20 done with error = 1.24529e-09
Elapsed time: 0.0915089 (iteration step)
Iteration 21 done with error = 3.73041e-10
Elapsed time: 0.0612509 (iteration step)
Iteration 22 done with error = 1.04037e-10
Elapsed time: 0.0602911 (iteration step)
Iteration 23 done with error = 3.09354e-11
Elapsed time: 0.0613561 (iteration step)
Source integral: 1.71189e-11
Elapsed time: 1.8938 (Solving 1st refinement step)

###################################
Number of mesh refinements: 2
###################################
Number of mesh elements (ff): 400
Number of mesh edges (ff): 400
Number of mesh nodes (ff): 401
Number of mesh elements (pm): 32000
Number of mesh edges (pm): 48240
Number of mesh nodes (pm): 16241

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 800

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 1
       K = 1.8e-14
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
   gamma = 2
  radius = 4.3e-06
   theta = 0.3
--------------------------------------------------- 


Solving linear Darcy system of size <16241 x 16241>
Solving linear Stokes system of size <401 x 401>
Overall degrees of freedom <16642>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=5000

Iteration 1 done with error = 8254.16
Elapsed time: 0.357648 (iteration step)
Iteration 2 done with error = 1535.11
Elapsed time: 0.205808 (iteration step)
Iteration 3 done with error = 285.829
Elapsed time: 0.202062 (iteration step)
Iteration 4 done with error = 53.3067
Elapsed time: 0.201663 (iteration step)
