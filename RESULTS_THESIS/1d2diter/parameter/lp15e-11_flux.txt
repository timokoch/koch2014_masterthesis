LU method  |  Description                                                 
--------------------------------------------------------------------------
default    |  default LU solver                                           
umfpack    |  UMFPACK (Unsymmetric MultiFrontal sparse LU factorization)  
mumps      |  MUMPS (MUltifrontal Massively Parallel Sparse direct Solver)
petsc      |  PETSc builtin LU solver                                     

###################################
Number of mesh refinements: 0
###################################
Number of mesh elements (ff): 100
Number of mesh edges (ff): 100
Number of mesh nodes (ff): 101
Number of mesh elements (pm): 2000
Number of mesh edges (pm): 3060
Number of mesh nodes (pm): 1061

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 200

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 1.5e-11
   gamma = 2
  radius = 4.3e-06
   theta = 0.3
--------------------------------------------------- 


Solving linear Darcy system of size <1061 x 1061>
Solving linear Stokes system of size <101 x 101>
Overall degrees of freedom <1162>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=500

Iteration 1 done with error = 3752.81
Elapsed time: 0.0315261 (iteration step)
Iteration 2 done with error = 355.512
Elapsed time: 0.0192308 (iteration step)
Iteration 3 done with error = 34.0503
Elapsed time: 0.0228128 (iteration step)
Iteration 4 done with error = 3.34268
Elapsed time: 0.0199749 (iteration step)
Iteration 5 done with error = 0.348951
Elapsed time: 0.026819 (iteration step)
Iteration 6 done with error = 0.0419777
Elapsed time: 0.020457 (iteration step)
Iteration 7 done with error = 0.00638892
Elapsed time: 0.0304608 (iteration step)
Iteration 8 done with error = 0.00123364
Elapsed time: 0.032902 (iteration step)
Iteration 9 done with error = 0.00028112
Elapsed time: 0.0221319 (iteration step)
Iteration 10 done with error = 7.10382e-05
Elapsed time: 0.021008 (iteration step)
Iteration 11 done with error = 1.91392e-05
Elapsed time: 0.025737 (iteration step)
Iteration 12 done with error = 5.36312e-06
Elapsed time: 0.0229571 (iteration step)
Iteration 13 done with error = 1.53935e-06
Elapsed time: 0.0199208 (iteration step)
Iteration 14 done with error = 4.48438e-07
Elapsed time: 0.0211539 (iteration step)
Iteration 15 done with error = 1.31868e-07
Elapsed time: 0.0198309 (iteration step)
Iteration 16 done with error = 3.90125e-08
Elapsed time: 0.020488 (iteration step)
Iteration 17 done with error = 1.15901e-08
Elapsed time: 0.021518 (iteration step)
Iteration 18 done with error = 3.44757e-09
Elapsed time: 0.0299561 (iteration step)
Iteration 19 done with error = 1.02924e-09
Elapsed time: 0.022131 (iteration step)
Iteration 20 done with error = 3.04838e-10
Elapsed time: 0.0213821 (iteration step)
Iteration 21 done with error = 9.10262e-11
Elapsed time: 0.022543 (iteration step)
Source integral: 7.6743e-12
Elapsed time: 0.852428 (Solving 0th refinement step)

###################################
Number of mesh refinements: 1
###################################
Number of mesh elements (ff): 200
Number of mesh edges (ff): 200
Number of mesh nodes (ff): 201
Number of mesh elements (pm): 8000
Number of mesh edges (pm): 12120
Number of mesh nodes (pm): 4121

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 400

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 1.5e-11
   gamma = 2
  radius = 4.3e-06
   theta = 0.3
--------------------------------------------------- 


Solving linear Darcy system of size <4121 x 4121>
Solving linear Stokes system of size <201 x 201>
Overall degrees of freedom <4322>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=500

Iteration 1 done with error = 7451.71
Elapsed time: 0.0921371 (iteration step)
Iteration 2 done with error = 705.372
Elapsed time: 0.0622301 (iteration step)
Iteration 3 done with error = 67.4636
Elapsed time: 0.0618019 (iteration step)
Iteration 4 done with error = 6.60393
Elapsed time: 0.0625191 (iteration step)
Iteration 5 done with error = 0.685289
Elapsed time: 0.0632162 (iteration step)
Iteration 6 done with error = 0.0815518
Elapsed time: 0.065624 (iteration step)
Iteration 7 done with error = 0.0122612
Elapsed time: 0.0610421 (iteration step)
Iteration 8 done with error = 0.00235243
Elapsed time: 0.062922 (iteration step)
Iteration 9 done with error = 0.000536446
Elapsed time: 0.06322 (iteration step)
Iteration 10 done with error = 0.000136238
Elapsed time: 0.0642331 (iteration step)
Iteration 11 done with error = 3.69444e-05
Elapsed time: 0.0621312 (iteration step)
Iteration 12 done with error = 1.04173e-05
Elapsed time: 0.0656631 (iteration step)
Iteration 13 done with error = 3.00599e-06
Elapsed time: 0.0624871 (iteration step)
Iteration 14 done with error = 8.79457e-07
Elapsed time: 0.063365 (iteration step)
Iteration 15 done with error = 2.59492e-07
Elapsed time: 0.063144 (iteration step)
Iteration 16 done with error = 7.6969e-08
Elapsed time: 0.0589731 (iteration step)
Iteration 17 done with error = 2.29071e-08
Elapsed time: 0.0610931 (iteration step)
Iteration 18 done with error = 6.83247e-09
Elapsed time: 0.066783 (iteration step)
Iteration 19 done with error = 2.03652e-09
Elapsed time: 0.058862 (iteration step)
Iteration 20 done with error = 6.10537e-10
Elapsed time: 0.059582 (iteration step)
Iteration 21 done with error = 1.78818e-10
Elapsed time: 0.0610971 (iteration step)
Iteration 22 done with error = 5.06874e-11
Elapsed time: 0.0819268 (iteration step)
Source integral: 7.6743e-12
Elapsed time: 1.82712 (Solving 1st refinement step)

###################################
Number of mesh refinements: 2
###################################
Number of mesh elements (ff): 400
Number of mesh edges (ff): 400
Number of mesh nodes (ff): 401
Number of mesh elements (pm): 32000
Number of mesh edges (pm): 48240
Number of mesh nodes (pm): 16241

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 800

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 1.5e-11
   gamma = 2
  radius = 4.3e-06
   theta = 0.3
--------------------------------------------------- 


Solving linear Darcy system of size <16241 x 16241>
Solving linear Stokes system of size <401 x 401>
Overall degrees of freedom <16642>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=500

Iteration 1 done with error = 14857.5
Elapsed time: 0.365179 (iteration step)
Iteration 2 done with error = 1405.87
Elapsed time: 0.211831 (iteration step)
