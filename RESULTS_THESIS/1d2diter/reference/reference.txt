LU method  |  Description                                                 
--------------------------------------------------------------------------
default    |  default LU solver                                           
umfpack    |  UMFPACK (Unsymmetric MultiFrontal sparse LU factorization)  
mumps      |  MUMPS (MUltifrontal Massively Parallel Sparse direct Solver)
petsc      |  PETSc builtin LU solver                                     
Elapsed time: 0.0569341 (Init MPI)
Ordering mesh.
Elapsed time: 0.00437999 (compute entities dim = 1)
Requesting connectivity 0 - 0.
Elapsed time: 0.000259876 (compute connectivity 0 - 0)
Requesting connectivity 0 - 1.
Requesting connectivity 1 - 0.
Computing mesh connectivity 0 - 1 from transpose.
Elapsed time: 0.000288963 (compute connectivity 0 - 1)
Requesting connectivity 0 - 2.
Requesting connectivity 2 - 0.
Computing mesh connectivity 0 - 2 from transpose.
Elapsed time: 0.000206947 (compute connectivity 0 - 2)
Requesting connectivity 1 - 1.
Requesting connectivity 1 - 0.
Requesting connectivity 0 - 1.
Computing mesh connectivity 1 - 1 from intersection 1 - 0 - 1.
Elapsed time: 0.002069 (compute connectivity 1 - 1)
Requesting connectivity 1 - 2.
Requesting connectivity 2 - 1.
Computing mesh connectivity 1 - 2 from transpose.
Elapsed time: 0.000218153 (compute connectivity 1 - 2)
Requesting connectivity 2 - 2.
Requesting connectivity 2 - 0.
Requesting connectivity 0 - 2.
Computing mesh connectivity 2 - 2 from intersection 2 - 0 - 2.
Elapsed time: 0.00152016 (compute connectivity 2 - 2)
Requesting connectivity 0 - 0.
Elapsed time: 2.59876e-05 (compute connectivity 0 - 0)
Requesting connectivity 0 - 1.
Requesting connectivity 1 - 0.
Computing mesh connectivity 0 - 1 from transpose.
Elapsed time: 2.98023e-05 (compute connectivity 0 - 1)
Requesting connectivity 1 - 1.
Requesting connectivity 1 - 0.
Requesting connectivity 0 - 1.
Computing mesh connectivity 1 - 1 from intersection 1 - 0 - 1.
Elapsed time: 0.000263929 (compute connectivity 1 - 1)

###################################
Number of mesh refinements: 0
###################################
Saved mesh mesh (DOLFIN mesh) to file results/mesh_0_refinement.pvd in VTK format.
Elapsed time: 0.00846815 (Write mesh to PVD/VTK file)
Saved mesh mesh (DOLFIN mesh) to file results/mesh_ff_0_refinement.pvd in VTK format.
Elapsed time: 0.00175786 (Write mesh to PVD/VTK file)

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 200
Computing sub domain markers for sub domain 1.
Computing sub domain markers for sub domain 1.
Saved mesh function mesh (DOLFIN mesh) to file results/facet_domains.pvd in VTK format.
Computing sub domain markers for sub domain 0.
Computing sub domain markers for sub domain 1.
Saved mesh function mesh (DOLFIN mesh) to file results/vertex_domains_ff.pvd in VTK format.
Elapsed time: 9.53674e-07 (Build mesh number mesh entities)
Elapsed time: 0 (Build mesh number mesh entities)
Elapsed time: 7.10487e-05 (Init dofmap from UFC dofmap)
Elapsed time: 0.000165939 (SCOTCH graph ordering)
Elapsed time: 0.000351906 (Init dofmap)
Elapsed time: 9.53674e-07 (Build mesh number mesh entities)
Elapsed time: 1.5974e-05 (Build mesh number mesh entities)
Elapsed time: 0.000576973 (Init dofmap from UFC dofmap)
Elapsed time: 0.000350952 (SCOTCH graph ordering)
Elapsed time: 0.00220084 (Init dofmap)

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
   gamma = 2
  radius = 4.3e-06
--------------------------------------------------- 

Initializing PETSc (ignoring command-line arguments).
Elapsed time: 0.000605106 (Init PETSc)
Matrix of size 4121 x 4121 has 46481 (0.273697) nonzero entries.
Elapsed time: 0.00665593 (Build sparsity)
Elapsed time: 0.000686169 (Init tensor)
Elapsed time: 9.53674e-07 (Delete sparsity)
Elapsed time: 0.00431705 (Assemble cells)
Elapsed time: 0.000203133 (Apply (PETScMatrix))
Matrix of size 201 x 201 has 801 (1.98262) nonzero entries.
Elapsed time: 0.000176191 (Build sparsity)
Elapsed time: 4.50611e-05 (Init tensor)
Elapsed time: 9.53674e-07 (Delete sparsity)
Elapsed time: 4.1008e-05 (Assemble cells)
Elapsed time: 8.10623e-06 (Apply (PETScMatrix))

Solving linear Darcy system of size <4121 x 4121>
Solving linear Stokes system of size <201 x 201>
Overall degrees of freedom <4322>
Elapsed time: 3.71933e-05 (Apply (PETScVector))
Elapsed time: 0.000279903 (Init dof vector)
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Elapsed time: 5.6982e-05 (Init dof vector)
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Elapsed time: 5.6982e-05 (Init dof vector)
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Elapsed time: 5.96046e-06 (Apply (PETScVector))
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=200

Elapsed time: 4.05312e-06 (Build sparsity)
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Elapsed time: 8.4877e-05 (Init tensor)
Elapsed time: 0 (Delete sparsity)
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Elapsed time: 0.000616074 (Assemble cells)
Elapsed time: 1.62125e-05 (Apply (PETScVector))
Computed bounding box tree with 199 nodes for 100 entities.
Computed bounding box tree with 3999 nodes for 2000 entities.
Applying point source to right-hand side vector.
Elapsed time: 2.7895e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.90735e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.69277e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.69277e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.90735e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.67708e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.71661e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 4.19617e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 3.91006e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.5974e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 4.31538e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.15256e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 8.82149e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 4.00543e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.5974e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 3.00407e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.81198e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.00136e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 4.76837e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 3.91006e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.15256e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 2.28882e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.19888e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.88351e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.19888e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.90735e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 6.19888e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.90735e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 1.78814e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 3.00407e-05 (Apply (PETScVector))
Applying point source to right-hand side vector.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Computing sub domain markers for sub domain 0.
Elapsed time: 0.000308037 (DirichletBC init facets)
Elapsed time: 0.000421047 (DirichletBC compute bc)
Applying boundary conditions to linear system.
Elapsed time: 9.05991e-06 (Apply (PETScVector))
Elapsed time: 5.6982e-05 (Apply (PETScMatrix))
Elapsed time: 0.000756979 (DirichletBC apply)
Computing sub domain markers for sub domain 0.
Elapsed time: 0.000235796 (DirichletBC init facets)
Elapsed time: 0.000313044 (DirichletBC compute bc)
Applying boundary conditions to linear system.
Elapsed time: 7.86781e-06 (Apply (PETScVector))
Elapsed time: 5.6982e-05 (Apply (PETScMatrix))
Elapsed time: 0.000540018 (DirichletBC apply)
Solving linear system of size 4121 x 4121 (PETSc LU solver, (null)).
Elapsed time: 0.0297401 (PETSc LU solver)
Elapsed time: 0.0297978 (LU solver)
Saved function u (a function) to file results/pressure_pm_0_refinement.pvd in VTK format.
Elapsed time: 6.91414e-06 (Build sparsity)
Elapsed time: 1.21593e-05 (Apply (PETScVector))
Elapsed time: 0.00011301 (Init tensor)
Elapsed time: 0 (Delete sparsity)
Elapsed time: 5.96046e-06 (Apply (PETScVector))
Elapsed time: 0.00176716 (Assemble cells)
Elapsed time: 7.15256e-06 (Apply (PETScVector))
Elapsed time: 3.09944e-06 (DirichletBC init facets)
Elapsed time: 2.5034e-05 (DirichletBC compute bc)
Applying boundary conditions to linear system.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Elapsed time: 2.86102e-06 (Apply (PETScMatrix))
Elapsed time: 0.000110865 (DirichletBC apply)
Elapsed time: 2.14577e-06 (DirichletBC init facets)
Elapsed time: 2.00272e-05 (DirichletBC compute bc)
Applying boundary conditions to linear system.
Elapsed time: 5.00679e-06 (Apply (PETScVector))
Elapsed time: 2.86102e-06 (Apply (PETScMatrix))
Elapsed time: 9.29832e-05 (DirichletBC apply)
Solving linear system of size 201 x 201 (PETSc LU solver, (null)).
Elapsed time: 0.000642061 (PETSc LU solver)
Elapsed time: 0.000669003 (LU solver)
Saved function p_ff (vessel pressure) to file results/pressure_ff_0_refinement.pvd in VTK format.
Elapsed time: 6.91414e-06 (Apply (PETScVector))
Elapsed time: 7.20024e-05 (Init dof vector)
Iteration 1 done with error = 21530.3
Elapsed time: 0.0787849 (iteration step)
Iteration 2 done with error = 20862.5
Elapsed time: 0.0226979 (iteration step)
Iteration 3 done with error = 2455.2
Elapsed time: 0.0222101 (iteration step)
Iteration 4 done with error = 293.526
Elapsed time: 0.0220542 (iteration step)
Iteration 5 done with error = 35.3463
Elapsed time: 0.0222909 (iteration step)
Iteration 6 done with error = 4.28657
Elapsed time: 0.0246539 (iteration step)
Iteration 7 done with error = 0.522373
Elapsed time: 0.0224569 (iteration step)
Iteration 8 done with error = 0.0640319
Elapsed time: 0.032707 (iteration step)
Iteration 9 done with error = 0.00787447
Elapsed time: 0.036216 (iteration step)
Iteration 10 done with error = 0.000978194
Elapsed time: 0.026207 (iteration step)
Iteration 11 done with error = 0.000123374
Elapsed time: 0.025614 (iteration step)
Iteration 12 done with error = 1.66519e-05
Elapsed time: 0.0228291 (iteration step)
Iteration 13 done with error = 2.61525e-06
Elapsed time: 0.026814 (iteration step)
Iteration 14 done with error = 5.26033e-07
Elapsed time: 0.0239689 (iteration step)
Iteration 15 done with error = 1.24503e-07
Elapsed time: 0.0264499 (iteration step)
Iteration 16 done with error = 3.17261e-08
Elapsed time: 0.0218718 (iteration step)
Iteration 17 done with error = 8.28832e-09
Elapsed time: 0.032649 (iteration step)
Iteration 18 done with error = 2.19695e-09
Elapsed time: 0.0295341 (iteration step)
Iteration 19 done with error = 5.87405e-10
Elapsed time: 0.0396631 (iteration step)
Iteration 20 done with error = 1.58592e-10
Elapsed time: 0.0232489 (iteration step)
Iteration 21 done with error = 4.37776e-11
Elapsed time: 0.0246489 (iteration step)
Source integral: 1.24603e-11
Elapsed time: 1.20349 (Solving 0th refinement step)

###################################
Number of mesh refinements: 1
###################################

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 400

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
   gamma = 2
  radius = 4.3e-06
--------------------------------------------------- 


Solving linear Darcy system of size <16241 x 16241>
Solving linear Stokes system of size <401 x 401>
Overall degrees of freedom <16642>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=200

Iteration 1 done with error = 42986.5
Elapsed time: 0.252744 (iteration step)
Iteration 2 done with error = 41624.3
Elapsed time: 0.0685191 (iteration step)
Iteration 3 done with error = 4900.16
Elapsed time: 0.0675609 (iteration step)
Iteration 4 done with error = 585.826
Elapsed time: 0.069947 (iteration step)
Iteration 5 done with error = 70.5579
Elapsed time: 0.0684478 (iteration step)
Iteration 6 done with error = 8.556
Elapsed time: 0.0677969 (iteration step)
Iteration 7 done with error = 1.04291
Elapsed time: 0.102021 (iteration step)
Iteration 8 done with error = 0.127797
Elapsed time: 0.0782549 (iteration step)
Iteration 9 done with error = 0.0157219
Elapsed time: 0.0692179 (iteration step)
Iteration 10 done with error = 0.00195014
Elapsed time: 0.082113 (iteration step)
Iteration 11 done with error = 0.000245629
Elapsed time: 0.071568 (iteration step)
Iteration 12 done with error = 3.27757e-05
Elapsed time: 0.069505 (iteration step)
Iteration 13 done with error = 5.0426e-06
Elapsed time: 0.0682151 (iteration step)
Iteration 14 done with error = 9.84868e-07
Elapsed time: 0.0687959 (iteration step)
Iteration 15 done with error = 2.29366e-07
Elapsed time: 0.0689778 (iteration step)
Iteration 16 done with error = 5.78781e-08
Elapsed time: 0.070313 (iteration step)
Iteration 17 done with error = 1.50298e-08
Elapsed time: 0.0725899 (iteration step)
Iteration 18 done with error = 3.96128e-09
Elapsed time: 0.070719 (iteration step)
Iteration 19 done with error = 1.05448e-09
Elapsed time: 0.111567 (iteration step)
Iteration 20 done with error = 2.83549e-10
Elapsed time: 0.0708511 (iteration step)
Iteration 21 done with error = 7.96957e-11
Elapsed time: 0.070507 (iteration step)
Source integral: 1.24603e-11
Elapsed time: 3.63654 (Solving 1st refinement step)

###################################
Number of mesh refinements: 2
###################################

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 800

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
   gamma = 2
  radius = 4.3e-06
--------------------------------------------------- 


Solving linear Darcy system of size <64481 x 64481>
Solving linear Stokes system of size <801 x 801>
Overall degrees of freedom <65282>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=200

Iteration 1 done with error = 85909.2
Elapsed time: 1.14914 (iteration step)
Iteration 2 done with error = 83158
Elapsed time: 0.250253 (iteration step)
Iteration 3 done with error = 9791.31
Elapsed time: 0.247446 (iteration step)
Iteration 4 done with error = 1170.57
Elapsed time: 0.327161 (iteration step)
Iteration 5 done with error = 140.999
Elapsed time: 0.442916 (iteration step)
Iteration 6 done with error = 17.0971
Elapsed time: 0.352061 (iteration step)
Iteration 7 done with error = 2.08425
Elapsed time: 0.252078 (iteration step)
Iteration 8 done with error = 0.255362
Elapsed time: 0.25398 (iteration step)
Iteration 9 done with error = 0.0314208
Elapsed time: 0.252682 (iteration step)
Iteration 10 done with error = 0.00389483
Elapsed time: 0.245119 (iteration step)
Iteration 11 done with error = 0.000490321
Elapsed time: 0.25563 (iteration step)
Iteration 12 done with error = 6.50986e-05
Elapsed time: 0.245585 (iteration step)
Iteration 13 done with error = 9.92428e-06
Elapsed time: 0.245183 (iteration step)
Iteration 14 done with error = 1.91075e-06
Elapsed time: 0.2854 (iteration step)
Iteration 15 done with error = 4.41337e-07
Elapsed time: 0.248377 (iteration step)
Iteration 16 done with error = 1.10727e-07
Elapsed time: 0.251543 (iteration step)
Iteration 17 done with error = 2.86415e-08
Elapsed time: 0.250756 (iteration step)
Iteration 18 done with error = 7.52305e-09
Elapsed time: 0.262174 (iteration step)
Iteration 19 done with error = 1.99371e-09
Elapsed time: 0.251122 (iteration step)
Iteration 20 done with error = 5.38203e-10
Elapsed time: 0.255734 (iteration step)
Iteration 21 done with error = 1.46756e-10
Elapsed time: 0.280466 (iteration step)
Iteration 22 done with error = 5.13438e-11
Elapsed time: 0.249594 (iteration step)
Source integral: 1.24603e-11
Elapsed time: 14.8878 (Solving 2nd refinement step)

###################################
Number of mesh refinements: 3
###################################

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 1600

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
   gamma = 2
  radius = 4.3e-06
--------------------------------------------------- 


Solving linear Darcy system of size <256961 x 256961>
Solving linear Stokes system of size <1601 x 1601>
Overall degrees of freedom <258562>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=200

Iteration 1 done with error = 171760
Elapsed time: 5.74916 (iteration step)
Iteration 2 done with error = 166231
Elapsed time: 0.952516 (iteration step)
Iteration 3 done with error = 19574.2
Elapsed time: 0.967067 (iteration step)
Iteration 4 done with error = 2340.14
Elapsed time: 0.986683 (iteration step)
Iteration 5 done with error = 281.889
Elapsed time: 0.943893 (iteration step)
Iteration 6 done with error = 34.1802
Elapsed time: 0.949386 (iteration step)
Iteration 7 done with error = 4.16706
Elapsed time: 1.05075 (iteration step)
Iteration 8 done with error = 0.510508
Elapsed time: 0.918791 (iteration step)
Iteration 9 done with error = 0.062821
Elapsed time: 1.00161 (iteration step)
Iteration 10 done with error = 0.00778461
Elapsed time: 0.91035 (iteration step)
Iteration 11 done with error = 0.000979849
Elapsed time: 0.986646 (iteration step)
Iteration 12 done with error = 0.000129802
Elapsed time: 0.955524 (iteration step)
Iteration 13 done with error = 1.97149e-05
Elapsed time: 1.00903 (iteration step)
Iteration 14 done with error = 3.77196e-06
Elapsed time: 1.05855 (iteration step)
Iteration 15 done with error = 8.68243e-07
Elapsed time: 0.91152 (iteration step)
Iteration 16 done with error = 2.17368e-07
Elapsed time: 0.911311 (iteration step)
Iteration 17 done with error = 5.61292e-08
Elapsed time: 0.90525 (iteration step)
Iteration 18 done with error = 1.47137e-08
Elapsed time: 0.912151 (iteration step)
Iteration 19 done with error = 3.90362e-09
Elapsed time: 0.913747 (iteration step)
Iteration 20 done with error = 1.04417e-09
Elapsed time: 0.901985 (iteration step)
Iteration 21 done with error = 2.87475e-10
Elapsed time: 0.910706 (iteration step)
Iteration 22 done with error = 9.98146e-11
Elapsed time: 0.902321 (iteration step)
Source integral: 1.24603e-11
Elapsed time: 88.33 (Solving 3rd refinement step)

###################################
Number of mesh refinements: 4
###################################

Chosen Gaussian integration with 2 integration points on interval [-1, 1] (x[0] = -0.57735, alpha[0] = 1); (x[1] = 0.57735, alpha[1] = 1);
Number of integration points in total: 3200

Parameters
--------------------------------------------------- 
mu_blood = 0.0028
   mu_if = 0.0013
       K = 6.5e-18
     K_m = 2.34e-20
     d_m = 6e-07
     L_p = 3e-11
   gamma = 2
  radius = 4.3e-06
--------------------------------------------------- 


Solving linear Darcy system of size <1025921 x 1025921>
Solving linear Stokes system of size <3201 x 3201>
Overall degrees of freedom <1029122>
starting iterative algorithm with parameters: tolerance=1e-10; max_iter=200

