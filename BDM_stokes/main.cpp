#include <dolfin.h>
#include "Stokes.h"
#include "Error.h"
#include "Errorp.h"

using namespace dolfin;


// Sub domain for inlet
class Inlet : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    double tol = 0.0001;
    return on_boundary && x[1] > (1 - tol);
  }
};

// Sub domain for outlet
class Outlet : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
     double tol = 0.0001;
     return on_boundary && x[1] < (-1 + tol);
  }
};

// Sub domain for wall
class Wall : public SubDomain
{
   bool inside(const Array<double>& x, bool on_boundary) const
   {
      return on_boundary; //will be overwritten
   }
};

// Sub domain for wall
class Middle : public SubDomain
{
   bool inside(const Array<double>& x, bool on_boundary) const
   {
      return x[1] < 0.2 && x[1] > -0.2 && x[0] < 0.1 && x[0] > -0.1;
   }
};

class ExactV : public Expression
{
public:
    ExactV() : Expression(2) {}

    void eval(Array<double>& values, const Array<double>& x) const
    { 
      double v_max = 2;
      double a = 0.2; //x-coordinate of interface / interface_pos_x
      values[0] = 0.0;
      values[1] = -v_max + (v_max)/(a*a)*x[0]*x[0]; //parabolic profile with slip velocity
    }

};

class ExactP : public Expression
{
public:
    ExactP() : Expression() {}

    void eval(Array<double>& values, const Array<double>& x) const
    { 
      values[0] = 100*(1+x[1]);
    }

};

int main()
{

  set_log_level(30);

  double error_old;  
  double errorp_old;

  for (int i = 1; i < 7; ++i)
  {
    // Mesh
    RectangleMesh mesh_old(-0.2, -1, 0.2, 1, pow(2,i-1), 5*pow(2,i-1), "left/right"); 
    RectangleMesh mesh(-0.2, -1, 0.2, 1, pow(2,i), 5*pow(2,i), "left/right"); 
    const int dim = mesh.topology().dim();
    mesh.init(dim-1, dim);

    // Create subdomains for boundary conditions
    Inlet inlet;
    Outlet outlet;
    Wall wall;

    // Mesh functions
    std::shared_ptr<MeshFunction<std::size_t> > exterior_facets_domains (new MeshFunction<std::size_t>(mesh, dim-1));

    // Mark subdomains
    exterior_facets_domains->set_all(3);
    wall.mark(*exterior_facets_domains, 2);
    inlet.mark(*exterior_facets_domains, 0);
    outlet.mark(*exterior_facets_domains, 1);

    File domainsfile("results/exterior_facets_domains.pvd");
    domainsfile << *exterior_facets_domains;

    // Create function space and subspaces
    Stokes::FunctionSpace W(mesh);
    SubSpace W0(W, 0);
    SubSpace W1(W, 1);

    // Define variational problem 
    Constant alpha(10);

    Constant pbar_out(0.0);
    Constant pbar_in(200.0);
    Constant mu(1);
    Constant zero(0.0);
    Stokes::BilinearForm a0(W, W);
    a0.alpha = alpha; 
    a0.mu = mu;
    Stokes::LinearForm L0(W);
    L0.pbar_in = pbar_in; L0.pbar_out = pbar_out;

    a0.set_exterior_facet_domains(exterior_facets_domains);
    L0.set_exterior_facet_domains(exterior_facets_domains);

    // Compute solution
    Function w(W);
    solve(a0 == L0, w);
    Function u = w[0];
    Function p = w[1];

    // Save solution in VTK format
    File ufile_pvd("results/velocity.pvd");
    ufile_pvd << u;
    File pfile_pvd("results/pressure.pvd");
    pfile_pvd << p;

    //Error calculation
    ExactP p_expression;
    ExactV v_expression;

    plot(u);
    plot(v_expression, mesh);
    plot(p);
    plot(p_expression);
    interactive();

    //High order spaces for error calculation
    Error::Functional E_V(mesh, u, v_expression);
    Errorp::Functional E_P(mesh, p, p_expression);
      
    double errorp = sqrt(assemble(E_P));
    double error = sqrt(assemble(E_V));

    double h_old = mesh_old.hmin();
    double h = mesh.hmin();

    std::cout << "h_min: " << h << std::endl;
    std::cout << "Error velocity: " << error << std::endl;
    std::cout << "Error pressure: " << errorp << std::endl;
    if(i>1){
      std::cout << "Experimental order of convergence (velocity): " << (log(error)-log(error_old))/(log(h)-log(h_old)) << std::endl;
      std::cout << "Experimental order of convergence (pressure): " << (log(errorp)-log(errorp_old))/(log(h)-log(h_old)) << std::endl;
    }
    std::cout << std::endl;
    errorp_old = errorp;
    error_old = error;
  }
  return 0;
}
