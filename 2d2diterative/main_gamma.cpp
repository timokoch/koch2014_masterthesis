// Stokes Darcy coupled problem in 2D
#include <dolfin.h>
#include "Stokes.h"
#include "Darcy.h"
#include "Gradient.h"
#include "Flux.h"
#include <boost/lexical_cast.hpp>

using namespace dolfin;

//#include "../subdomains/Bifurcation.h"
#include "../subdomains/RealRectangle.h"
//#include "../subdomains/Circle.h"

// Gives back the function on the interface as expression
class Interface_coupling_scalar : public Expression
{
public:
	const Function & f;
	Interface & interface;
	Interface_coupling_scalar(const Function & u, Interface & coupling_interface) : f(u), interface(coupling_interface), Expression() {}

	void eval(Array<double>& values, const Array<double>& x, const ufc::cell& ufc_cell) const
	{
		if(interface.inside(x, true))
			f.eval(values, x, ufc_cell);
		else
		{	
			values[0] = 0.0;
		}
	}
};

// Gives back the function on the interface as expression
class Interface_coupling_vector : public Expression
{
public:
	const Function & f;
	Interface & interface;
	Interface_coupling_vector(const Function & u, Interface & coupling_interface) : f(u), interface(coupling_interface), Expression(2) {}

	void eval(Array<double>& values, const Array<double>& x, const ufc::cell& ufc_cell) const
	{
		if(interface.inside(x, true))
			f.eval(values, x, ufc_cell);
		else
		{
			values[0] = 0.0;
			values[1] = 0.0;
		}
	}
};

// Puts together functions on different domains/submeshes for output
class Output_vector : public Expression
{
public:
	const Function & u_ff;
	const Function & u_pm;
	StokesDomain & stokes_domain;
	DarcyDomain & darcy_domain;
	Output_vector(const Function & uff, const Function & upm, StokesDomain & stokes, DarcyDomain & darcy) : u_pm(upm), u_ff(uff), stokes_domain(stokes), darcy_domain(darcy), Expression(2) {}

	void eval(Array<double>& values, const Array<double>& x, const ufc::cell& ufc_cell) const
	{
		if(stokes_domain.inside(x, true) || stokes_domain.inside(x, false))
			u_ff.eval(values, x, ufc_cell);
		else
			u_pm.eval(values, x, ufc_cell);
	}
};

// Puts together functions on different domains/submeshes for output
class Output_scalar : public Expression
{
public:
	const Function & u_ff;
	const Function & u_pm;
	StokesDomain & stokes_domain;
	DarcyDomain & darcy_domain;
	Output_scalar(const Function & uff, const Function & upm, StokesDomain & stokes, DarcyDomain & darcy) : u_pm(upm), u_ff(uff), stokes_domain(stokes), darcy_domain(darcy), Expression() {}

	void eval(Array<double>& values, const Array<double>& x, const ufc::cell& ufc_cell) const
	{
		if(stokes_domain.inside(x, true))
			u_ff.eval(values, x, ufc_cell);
		else
			u_pm.eval(values, x, ufc_cell);
	}
};

// Initialize functions with a 0 exression
class Initialize_scalar_function : public Expression
{
public:

	Initialize_scalar_function() : Expression() {}

	void eval(Array<double>& values, const Array<double>& x) const
	{
		values[0] = 0;
	}

};

// Initialize functions with a 0 exression
class Init_Gamma : public Expression
{
public:

	Init_Gamma() : Expression() {}

	void eval(Array<double>& values, const Array<double>& x) const
	{
		values[0] = 0;
	}

};

// Initialize functions with a 0 exression
class Initialize_vector_function : public Expression
{
public:

	Initialize_vector_function() : Expression(2) {}

	void eval(Array<double>& values, const Array<double>& x) const
	{
		values[0] = 0;
		values[1] = 0;
	}

};

class Update_gamma : public Expression
{
public:
	const Function & p_pm;
	const Function & Gamma_k;
	const Function & u_ff_new;
	const Function & u_ff_old;
	Interface & interface;
	double gamma_pm;
	double gamma_ff;

	Update_gamma(const Function & f_p_pm, const Function & f_Gamma_k, const Function & f_u_ff_new, const Function & f_u_ff_old, double Gamma_pm, double Gamma_ff, Interface & coupling_interface) : p_pm(f_p_pm), Gamma_k(f_Gamma_k), u_ff_new(f_u_ff_new), u_ff_old(f_u_ff_old), gamma_pm(Gamma_pm), gamma_ff(Gamma_ff), interface(coupling_interface), Expression() {}

	void eval(Array<double>& values, const Array<double>& x, const ufc::cell& ufc_cell) const
	{

		if(interface.inside(x, true))
		{
			Array<double> values_p_pm(1);
			Array<double> values_Gamma_k(1);
			Array<double> values_u_ff_new(2);
			Array<double> values_u_ff_old(2);
			p_pm.eval(values_p_pm, x, ufc_cell);
			Gamma_k.eval(values_Gamma_k, x, ufc_cell);
			u_ff_new.eval(values_u_ff_new, x, ufc_cell);

			if (x[0]>0)
				values[0] = -gamma_ff/gamma_pm*values_Gamma_k[0] + (gamma_pm+gamma_ff)/gamma_pm*values_p_pm[0] + (gamma_pm + gamma_ff)*values_u_ff_new[0];
			else
				values[0] = -gamma_ff/gamma_pm*values_Gamma_k[0] + (gamma_pm+gamma_ff)/gamma_pm*values_p_pm[0] - (gamma_pm + gamma_ff)*values_u_ff_new[0];

		}	
		else
		{
			values[0] = 0.0;
		}
	}
};

std::string output_filename(std::string base_name, std::size_t index, std::string suffix_name)
{
	return base_name + "_" + boost::lexical_cast<std::string>( index ) + suffix_name;
}

std::string task_name(std::string base_name, std::size_t index, std::string suffix_name)
{
  std::vector<std::string> ordinal_({"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"});
  return base_name + boost::lexical_cast<std::string>(index) + ordinal_[index] + suffix_name;
}

int main() 
{
  	// Debug log level
	list_lu_solver_methods();
	list_krylov_solver_methods();
	list_krylov_solver_preconditioners(); 
  	parameters("lu_solver")["reuse_factorization"] = true;
  	parameters("lu_solver")["same_nonzero_pattern"] = true;
  	//info(parameters, true);
  	set_log_level(30);
	
	int refinements = 0;
	int num_gamma = 20;
	std::ofstream gamma_dat("gamma.dat");


	for (int gammaidx = 0; gammaidx < num_gamma; ++gammaidx)
	{
	
	for(std::size_t refidx=0; refidx<=refinements; refidx++) 
	{	
		Timer timer(task_name("Solving " , refidx, " refinement step"));
    	timer.start();
		// Read Mesh and initialise
		//Mesh mesh("../mesh/bifurcation.xml");
		Mesh mesh("../mesh/realrectangle.xml");
  		//Mesh mesh("../mesh/circle.xml");
		const int dim = mesh.topology().dim();

		if(refidx>0) 
			for (int i = 0; i < refidx; ++i)
				mesh = refine(mesh);

		mesh.init(dim-1, dim);

		std::cout << std::endl;
    	std::cout << "###################################" << std::endl;
    	std::cout << "Number of mesh refinements: " << refidx << std::endl;
    	std::cout << "###################################" << std::endl;

		File(output_filename("results/mesh", refidx, "_refinement.pvd")) << mesh;

  		// Initialise subdomain instances and markers
		StokesDomain vessel; DarcyDomain tissue; 
		Inflow inflow; InflowExceptInterfaceNode inflow_x; Outflow outflow; Noflow noflow; 
		Bottom bottom; Top top; Right right; Left left; 
		Interface interface; 

  		// Initialise submeshes
		SubMesh pm_mesh(mesh, tissue);
		//Mesh pm_mesh("mesh/pm_mesh.xml");
		SubMesh ff_mesh(mesh, vessel);

		std::cout << "Number of mesh elements (ff): " << ff_mesh.num_cells() << std::endl;
    	std::cout << "Number of mesh edges (ff): " << ff_mesh.num_edges() << std::endl;
    	std::cout << "Number of mesh nodes (ff): " << ff_mesh.num_vertices() << std::endl;
		std::cout << "Number of mesh elements (pm): " << pm_mesh.num_cells() << std::endl;
    	std::cout << "Number of mesh edges (pm): " << pm_mesh.num_edges() << std::endl;
    	std::cout << "Number of mesh nodes (pm): " << pm_mesh.num_vertices() << std::endl;


  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		// Subdomain marking //////////////////////////////////////////////////////////////////////////////////////////////////////
  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  		// Initialise MeshFunctions
		std::shared_ptr<MeshFunction<std::size_t> > extFacetDomains_pm (new MeshFunction<std::size_t>(pm_mesh, dim-1));
		std::shared_ptr<MeshFunction<std::size_t> > extFacetDomains_ff (new MeshFunction<std::size_t>(ff_mesh, dim-1));
		std::shared_ptr<MeshFunction<std::size_t> > extVertexDomains_pm (new MeshFunction<std::size_t>(pm_mesh, 0));
		std::shared_ptr<MeshFunction<std::size_t> > extVertexDomains_ff (new MeshFunction<std::size_t>(ff_mesh, 0));
		std::shared_ptr<MeshFunction<std::size_t> > interface_vertex (new MeshFunction<std::size_t>(mesh, 0));

		extFacetDomains_ff->set_all(0);
		extFacetDomains_pm->set_all(0);
		extVertexDomains_ff->set_all(0);
		extVertexDomains_pm->set_all(0);
		interface_vertex->set_all(0);

   		// Domain markers
		int wall = 1;
		int inlet = 2;
		int outlet = 3;
		int out = 4;
		int dirich = 5;

   		// Mark facets
		right.mark(*extFacetDomains_pm, dirich);
		left.mark(*extFacetDomains_pm, dirich);
		bottom.mark(*extFacetDomains_pm, out);
		top.mark(*extFacetDomains_pm, out);
		interface.mark(*extFacetDomains_pm, wall);
		interface.mark(*extFacetDomains_ff, wall);
		outflow.mark(*extFacetDomains_ff, outlet);
		inflow.mark(*extFacetDomains_ff, inlet);

		File pm_domains("results/extFacetDomains_pm.pvd"); pm_domains << *extFacetDomains_pm;
		File ff_domains("results/extFacetDomains_ff.pvd"); ff_domains << *extFacetDomains_ff;


   		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		// Create Problem /////////////////////////////////////////////////////////////////////////////////////////////////////////
  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		// Create Function Spaces
		Stokes::FunctionSpace W(ff_mesh);
		SubSpace W0(W, 0);
		SubSpace W1(W, 1);
		Darcy::FunctionSpace P(pm_mesh);

		// Physical coefficients
		double interface_pos_x = 0.1; //interface position at inlet
		double v_max = 6e-3; 
		double v_slip = 0.0;
		double K = 9e-19; //6.5e-18;
		double K_wall = 2.34e-20;
		double mu_blood = 2.8e-3; //2.8e-3;
		double mu_if = 1.3e-3; 
		double d = 0.6e-6;
		double l_p_1 = (mu_if*d)/K_wall;

		Constant c_mu_blood(mu_blood); Constant c_mu_if(mu_if); Constant c_K(K); 
		Constant L_p_1(l_p_1); 
		
		double gamma_pm_ = (0.1*gammaidx+0.1)*l_p_1;
		double gamma_ff_ = 3*gamma_pm_; 
		Constant gamma_pm(gamma_pm_);
		Constant gamma_ff(gamma_ff_);

		//Output parameters
    	std::cout << std::endl;
    	std::cout << "Parameters" << std::endl;
    	std::cout << "--------------------------------------------------- " << std::endl;
    	std::cout << "mu_blood = " << mu_blood << std::endl;
    	std::cout << "   mu_if = " << mu_if << std::endl;
    	std::cout << "       K = " << K << std::endl;
    	std::cout << "     K_m = " << K_wall << std::endl;
    	std::cout << "     d_m = " << d << std::endl;
    	std::cout << "     L_p = " << 1/l_p_1 << std::endl;
    	std::cout << "gamma_ff =  "<< gamma_ff_ << std::endl;
		std::cout << "gamma_pm =  "<< gamma_pm_ << std::endl;
    	std::cout << "--------------------------------------------------- " << std::endl;
    	std::cout << std::endl;

	  	// Darcy
	  	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		Constant pleft(-933); 
		Constant pright(-933);
		DirichletBC bcp1(P, pleft, left);
		DirichletBC bcp2(P, pright, right);
		std::vector<const DirichletBC*> bcd;
		bcd.push_back(&bcp1); bcd.push_back(&bcp2); 

	  	// Define variational problem
		Darcy::BilinearForm a1(P, P);
		a1.K = c_K; a1.mu = c_mu_if; a1.gamma_pm = gamma_pm; 
		Darcy::LinearForm L1(P);

		a1.set_exterior_facet_domains(extFacetDomains_pm);
		L1.set_exterior_facet_domains(extFacetDomains_pm);

		Function p_pm(P);
		std::shared_ptr<Matrix> A1(new Matrix());
    	std::shared_ptr<Vector> b1(new Vector());
		assemble(*A1, a1);

		// Stokes
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	Boundary conditions
		VelocityInScalar velocityIn(v_max, v_slip); // inlet Dirichlet boundary
		SubSpace VY(W0, 1);
		SubSpace VX(W0, 0);
		Constant zero(0.0);	
		//DirichletBC bc0(VY, zero, interface);
		//DirichletBC bc1(VX, zero, interface_hor);
		
		std::vector<const DirichletBC*> bcs;
	  	//bcs.push_back(&bc0); //bcs.push_back(&bc1); 

	  	// Define variational problem
		Constant c_pbar_out(-1600); 
		Constant c_pbar_in(400); 

		Stokes::BilinearForm a0(W, W);
		a0.mu = c_mu_blood; a0.gamma_ff = gamma_ff; a0.L_p_1 = L_p_1; std::cout << "L_p^-1 =  "<< l_p_1 << std::endl;
		Stokes::LinearForm L0(W);
		L0.pbar_out = c_pbar_out; L0.gamma_ff = gamma_ff; L0.gamma_pm = gamma_pm; L0.pbar_in = c_pbar_in;

		a0.set_exterior_facet_domains(extFacetDomains_ff);
		L0.set_exterior_facet_domains(extFacetDomains_ff);

		Function w(W);	
		std::shared_ptr<Matrix> A0(new Matrix());
    	std::shared_ptr<Vector> b0(new Vector());	
		assemble(*A0, a0);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  
		//Output matrix sizes
		std::cout << std::endl;
    	std::cout << "Solving linear Darcy pressure system of size <" << A1->size(0) << " x " << A1->size(1) << ">" << std::endl;
    	std::cout << "Solving linear Stokes system of size <" << A0->size(0) << " x " << A0->size(1) << ">" << std::endl;
    	std::cout << "Overall degrees of freedom <" << A0->size(0)+A1->size(0) << ">" << std::endl;


		// Iteration algorithm
		double error = 1.0;
		double tol = 1e-8; 
		int iterations = 0;
		int max_iter = 100;

		std::cout << "tolerance =  "<< tol << std::endl;

		// Output files
		File velocity_ff_file(output_filename("results/velocity_ff", refidx, "_refinement.pvd"));
		File pressure_ff_file(output_filename("results/pressure_ff", refidx, "_refinement.pvd"));
		File velocity_pm_file(output_filename("results/velocity_pm", refidx, "_refinement.pvd"));
		File pressure_pm_file(output_filename("results/pressure_pm", refidx, "_refinement.pvd"));
		
		// Functions used inside the iteration algorithm
		Function p_pm_old(P);
		Function w_old(W);
		Function u_ff_old = w_old[0];
		Function p_ff_old = w_old[1];

		Function p_pm_new(P);
		Function u_ff_new = w_old[0];
		Function p_ff_new = w_old[1];

		Stokes::CoefficientSpace_p_pm Q(ff_mesh);
		Function p_pm_to_stokes(Q);

		// Initialize functions
		Initialize_scalar_function init_scalar;
		Initialize_vector_function init_vector;
		p_pm_old = init_scalar;  p_ff_old = init_scalar; p_pm_new = init_scalar; 
		p_ff_new = init_scalar; u_ff_old = init_vector; u_ff_new = init_vector;

		Stokes::FunctionSpace W_MESH(mesh);
		Darcy::FunctionSpace P_MESH(mesh); 
		Function Gamma_k(P_MESH);
		Function Gamma_k_old(P_MESH);
		Init_Gamma init_gamma;
		Gamma_k = init_gamma;
		Gamma_k_old = init_scalar;

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  	// Iterative solver ///////////////////////////////////////////////////////////////////////////////////////////////////////
	  	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		LUSolver darcy_solver(A1, "mumps");
		LUSolver stokes_solver(A0, "mumps");
		
		darcy_solver.parameters["reuse_factorization"] = true;
		stokes_solver.parameters["reuse_factorization"] = true;

		while (error > tol) {
			
			Timer timeriter("iteration step");
     		timeriter.start();
			
			++iterations;
			// If it doesn't converge throw error
			if (iterations >= max_iter) 
			{ 
				std::cout << "Iterative method didn't converge after " << max_iter << " steps!"; 
				break;
			}
			
			Timer timer_1("Update Gamma");
			Gamma_k_old = Gamma_k;
			Update_gamma Gamma_k_new(p_pm_new, Gamma_k_old, u_ff_new, u_ff_old, gamma_pm, gamma_ff, interface);
			Gamma_k = Gamma_k_new;
			set_log_level(0);
      		timer_1.stop();
      		set_log_level(30);
			
			Timer timer_2("Solve Darcy pressure");
			// Solve Darcy 
			L1.Gamma_k = Gamma_k;
			assemble(*b1, L1);
			for(std::size_t i = 0; i < bcd.size(); i++)
				bcd[i]->apply(*A1, *b1);
			darcy_solver.solve(*p_pm.vector(), *b1);
			set_log_level(0);
      		timer_2.stop();
      		set_log_level(30);
			
			Timer timer_4("Extrapolate pressure to ff_mesh mesh via expression class");
			// Extrapolate pressure to ff_mesh mesh via expression class
			Interface_coupling_scalar p_pm_expression(p_pm, interface);
			p_pm_to_stokes = p_pm_expression;
			set_log_level(0);
      		timer_4.stop();
      		set_log_level(30);

			Timer timer_5("Solve Stokes");
			// Solve Stokes 
			L0.p_pm = p_pm_to_stokes; L0.Gamma_k = Gamma_k;
			assemble(*b0, L0);
			for(std::size_t i = 0; i < bcs.size(); i++)
				bcs[i]->apply(*A0, *b0);
			stokes_solver.solve(*w.vector(), *b0);
			set_log_level(0);
      		timer_5.stop();
      		set_log_level(30);

      		// Split mixed function
			Function u_ff = w[0];	
			Function p_ff = w[1];	
			
			// Error calculation L2 norm
			error = 0;
			Function error_function_P(P);
			Function error_function_W(W);
			Function error_function_V = error_function_W[0];
			Function error_function_Q = error_function_W[1];

			// New step becomes old iteration step
			p_pm_old = p_pm_new;
			p_ff_old = p_ff_new;
			u_ff_old = u_ff_new;

			p_pm_new = p_pm;
			p_ff_new = p_ff;
			u_ff_new = u_ff;
			
			error_function_P = p_pm_new;
			*error_function_P.vector() -= *p_pm_old.vector();
			error += error_function_P.vector()->norm("l2");
			
			error_function_Q = p_ff_new;
			*error_function_Q.vector() -= *p_ff_old.vector();
			error += error_function_Q.vector()->norm("l2");
			
			error_function_V = u_ff_new;
			*error_function_V.vector() -= *u_ff_old.vector();
			error += error_function_V.vector()->norm("l2");

			// Print number of iterations and error
			std::cout << "iteration " << iterations << " done with error = " << error << std::endl;

			set_log_level(0);
    		timeriter.stop();
      		set_log_level(30);
		}

		gamma_dat << gamma_pm_ << " " << iterations << "\n";

		set_log_level(0);
      	timer.stop();
      	set_log_level(30);
	}
	}

	gamma_dat.close();


	return 0;
}