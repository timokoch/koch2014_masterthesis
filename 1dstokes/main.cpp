#include <dolfin.h>
#include "Stokes.h"

using namespace dolfin;


// Sub domain for inlet
class Inlet : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[0], -0.5e-3);
  }
};

// Sub domain for outlet
class Outlet : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
     return on_boundary && near(x[0], 0.5e-3);
  }
};

class InterstitialPressure : public Expression
{
public:
  double c;
  InterstitialPressure(double c_mom) : c(c_mom), Expression() {}

  void eval(Array<double>& values, const Array<double>& x) const
 {
    double gradient = 0;
    double mean = -933;
    values[0] = gradient*x[0] + mean;
 }
};

class PressureOut : public Expression
{
public:
  void eval(Array<double>& values, const Array<double>& x) const
 {
    double gradient = 0;
    double mean = 0;
    values[0] = gradient*x[0] + mean;
 }
};

int main()
{

  set_log_level(0);

  // Read mesh
  //Mesh mesh("1dvessel.xml");
  IntervalMesh mesh(1000, -0.5e-3, 0.5e-3);

  Inlet inlet;
  Outlet outlet;
  
  const int dim = mesh.topology().dim();

  boost::shared_ptr<MeshFunction<std::size_t> > domains(new MeshFunction<std::size_t> (mesh, dim-1));

  domains->set_all(2);
  int in = 0;
  int out = 1;
  inlet.mark(*domains, in);
  outlet.mark(*domains, out);

  File domainsfile("results/domains.pvd");
  domainsfile << *domains;

  // Create function space and subspaces
  Stokes::FunctionSpace P(mesh);

  // Create functions for boundary conditions
  double p_out = -1600;
  double p_in = 400;
  Constant pressure_in(p_in);
  Constant pressure_out(p_out);

  //PressureOut pressure_out;

  // Boundary conditions
  DirichletBC bc0(P, pressure_in, *domains, in);
  DirichletBC bc1(P, pressure_out, *domains, out);

  // Collect boundary conditions
  std::vector<const DirichletBC*> bcs;
  bcs.push_back(&bc0); bcs.push_back(&bc1); 

  // Define variational problem 
  // Coefficients vessel
  double density = 1045;
  double mu_blood = 2.8e-3;
  double nu_blood = mu_blood/density;
  double gamma = 2;
  double radius = 2.15e-6;
  double area = 2*DOLFIN_PI*radius*radius;
  // Coefficients membrane
  double K_m = 2.34e-15;
  double mu_if = 1.3e-3;
  double d_m = 0.6e-6;

  // Summed up coefficients
  double c_mass = area/(2*DOLFIN_PI*radius*K_m/(mu_if*d_m));
  double c_mom = area/(density*2*DOLFIN_PI*nu_blood*(2+gamma)); 

  InterstitialPressure p_pm(c_mom);
  Constant C_mass(c_mass);
  Constant C_mom(c_mom);
  
  std::cout << "c_mass:   " << c_mass << std::endl;
  std::cout << "c_mom:    " << c_mom << std::endl;
  p_pm.rename("p_pm", "interstitial pressure");
  plot(p_pm, mesh);

  Stokes::BilinearForm a0(P, P);
  a0.C_mass = C_mass; a0.C_mom = C_mom; 

  Stokes::LinearForm L0(P);
  L0.p_pm = p_pm;

  a0.set_exterior_facet_domains(domains);
  L0.set_exterior_facet_domains(domains);

  // Compute solution
  Function p_ff(P);
  solve(a0 == L0, p_ff, bcs);
  
  p_ff.rename("p", "free-flow pressure");

  plot(p_ff);
  interactive();

  File pfile_pvd("results/pressure.pvd");
  pfile_pvd << p_ff;
  
  return 0;
}
