// SUBDOMAINS ////
// for realistic mesh 
class SubDomainRectangle : public SubDomain
{
	public:
		const double x_min = -105.0e-6;
		const double x_max = 105.0e-6;
		const double y_min = -2e-3/DOLFIN_PI;
		const double y_max = 2e-3/DOLFIN_PI;
		const double interface_pos_x = 4.3e-6;
		const double tol = 1e-9;
};

// Function for inflow boundary condition for velocity
class VelocityInScalar : public Expression
{
public:
  	double v_max;
  	double v_slip;
  	VelocityInScalar(double max, double slip) : v_max(max), v_slip(slip), Expression() {}

  	void eval(Array<double>& values, const Array<double>& x) const
  	{	
  		double a = 4.3e-6; //x-coordinate of interface / interface_pos_x
    	values[0] = -v_max + (v_max-v_slip)/(a*a)*x[0]*x[0]; //parabolic profile with slip velocity
  	}

};

// Function for inflow boundary condition for velocity
class VelocityIn : public Expression
{
public:
  	double v_max;
  	double v_slip;
  	VelocityIn(double max, double slip) : v_max(max), v_slip(slip), Expression(2) {}

  	void eval(Array<double>& values, const Array<double>& x) const
  	{	
  		double a = 4.3e-6; //x-coordinate of interface / interface_pos_x
    	values[0] = 0.0;
    	values[1] = -v_max + (v_max-v_slip)/(a*a)*x[0]*x[0]; //parabolic profile with slip velocity
  	}

};

// Define subdomain classes
class StokesDomain : public SubDomainRectangle
{
public:
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return (x[0]-this->y_max)*(x[0]-this->y_max) + (x[1]-this->y_max)*(x[1]-this->y_max) > (this->y_max-this->interface_pos_x)*(this->y_max-this->interface_pos_x) - this->tol && \
				(x[0]-this->y_max)*(x[0]-this->y_max) + (x[1]-this->y_max)*(x[1]-this->y_max) < (this->y_max+this->interface_pos_x)*(this->y_max+this->interface_pos_x) + this->tol;
	}
};

class DarcyDomain : public SubDomainRectangle
{
public:
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return (x[0]-this->y_max)*(x[0]-this->y_max) + (x[1]-this->y_max)*(x[1]-this->y_max) < (this->y_max-this->interface_pos_x)*(this->y_max-this->interface_pos_x) + this->tol || \
				(x[0]-this->y_max)*(x[0]-this->y_max) + (x[1]-this->y_max)*(x[1]-this->y_max) > (this->y_max+this->interface_pos_x)*(this->y_max+this->interface_pos_x) - this->tol;
	}
};

class Inflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && x[0] <= this->interface_pos_x && x[0] >= -1*this->interface_pos_x && near(x[1], this->y_max);
	}
};

class InflowExceptInterfaceNode : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && x[0] < this->interface_pos_x && x[0] > -1*this->interface_pos_x && x[1] > 0;
	}
};

class Outflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && x[1] <= this->interface_pos_x && x[1] >= -1*this->interface_pos_x && near(x[0], this->y_max);
	}
};

class Bottom : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[0], this->y_max);
	}
};

class Top : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[1], this->y_max);
	}
};

class Right : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && (x[0]-this->y_max)*(x[0]-this->y_max) + (x[1]-this->y_max)*(x[1]-this->y_max) < (this->y_max-this->x_max)*(this->y_max-this->x_max) + this->tol;
	}
};

class Left : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && (x[0]-this->y_max)*(x[0]-this->y_max) + (x[1]-this->y_max)*(x[1]-this->y_max) > (this->y_max+this->x_max)*(this->y_max+this->x_max) - this->tol;
	}
};

class Noflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary; //will be overwritten
	}
};

class Interface : public SubDomainRectangle
{
public:

	bool inside(const Array<double>& x, bool on_boundary) const
	{	
		return on_boundary && (near((x[0]-this->y_max)*(x[0]-this->y_max) + (x[1]-this->y_max)*(x[1]-this->y_max), (this->y_max-this->interface_pos_x)*(this->y_max-this->interface_pos_x), this->tol) || \
				near((x[0]-this->y_max)*(x[0]-this->y_max) + (x[1]-this->y_max)*(x[1]-this->y_max), (this->y_max+this->interface_pos_x)*(this->y_max+this->interface_pos_x), this->tol));
	}
};

