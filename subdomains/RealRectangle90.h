// SUBDOMAINS ////
// for realistic mesh 
class SubDomainRectangle : public SubDomain
{
	public:
		const double x_min = -105.0e-6;
		const double x_max = 105.0e-6;
		const double y_min = -0.5e-3;
		const double y_max = 0.5e-3;
		const double interface_pos_x = 4.3e-6;
};

// Function for inflow boundary condition for velocity
class VelocityInScalar : public Expression
{
public:
  	double v_max;
  	double v_slip;
  	VelocityInScalar(double max, double slip) : v_max(max), v_slip(slip), Expression() {}

  	void eval(Array<double>& values, const Array<double>& x) const
  	{	
  		double a = 4.3e-6; //x-coordinate of interface / interface_pos_x
    	values[0] = -v_max + (v_max-v_slip)/(a*a)*x[0]*x[0]; //parabolic profile with slip velocity
  	}

};

// Function for inflow boundary condition for velocity
class VelocityIn : public Expression
{
public:
  	double v_max;
  	double v_slip;
  	VelocityIn(double max, double slip) : v_max(max), v_slip(slip), Expression(2) {}

  	void eval(Array<double>& values, const Array<double>& x) const
  	{	
  		double a = 4.3e-6; //x-coordinate of interface / interface_pos_x
    	values[0] = 0.0;
    	values[1] = -v_max + (v_max-v_slip)/(a*a)*x[0]*x[0]; //parabolic profile with slip velocity
  	}

};

// Define subdomain classes
class StokesDomain : public SubDomainRectangle
{
public:
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		if(x[1] > this->interface_pos_x)
			return x[0] <= this->interface_pos_x && x[0] >= -1*this->interface_pos_x;
		else
			return x[0] >= -1*this->interface_pos_x && x[1] >= -1*this->interface_pos_x && x[1] <= (this->interface_pos_x);
	}
};

class DarcyDomain : public SubDomainRectangle
{
public:
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return (x[0] <= -1*this->interface_pos_x) || (x[1]<= -1*this->interface_pos_x) || (x[0] >= this->interface_pos_x && x[1] >= this->interface_pos_x);
	}
};

class Inflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && x[0] <= this->interface_pos_x && x[0] >= -1*this->interface_pos_x && near(x[1], this->y_max);
	}
};

class InflowExceptInterfaceNode : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && x[0] < this->interface_pos_x && x[0] > -1*this->interface_pos_x && x[1] > 0;
	}
};

class Outflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && x[1] <= this->interface_pos_x && x[1] >= -1*this->interface_pos_x && near(x[0], this->y_max);
	}
};

class Bottom : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[0], this->y_max);
	}
};

class Top : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[1], this->y_max);
	}
};

class Right : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && ((x[1]>0 && near(x[0], this->x_max)) || (x[0]>0 && near(x[1], this->x_max)));
	}
};

class Left : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && (near(x[0], this->x_min) || near(x[1], this->x_min));
	}
};

class Noflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary; //will be overwritten
	}
};

class Interface : public SubDomainRectangle
{
public:

	bool inside(const Array<double>& x, bool on_boundary) const
	{	
		return (near(x[0], -1*this->interface_pos_x) && x[1] >= -1*this->interface_pos_x) || (near(x[1], -1*this->interface_pos_x) && x[0] >= -1*this->interface_pos_x) || (near(x[1], this->interface_pos_x) && x[0] >= this->interface_pos_x) || (near(x[0], this->interface_pos_x) && x[1] >= this->interface_pos_x);
	}
};

class InterfaceHorizontal : public SubDomainRectangle
{
public:

	bool inside(const Array<double>& x, bool on_boundary) const
	{	
		return (near(x[1], -1*this->interface_pos_x) && x[0] > -1*this->interface_pos_x) || (near(x[1], this->interface_pos_x) && x[0] >= this->interface_pos_x);
	}
};

class InterfaceVertical : public SubDomainRectangle
{
public:

	bool inside(const Array<double>& x, bool on_boundary) const
	{	
		return (near(x[0], -1*this->interface_pos_x) && x[1] > -1*this->interface_pos_x)  || (near(x[0], this->interface_pos_x) && x[1] >= this->interface_pos_x);
	}
};

