// SUBDOMAINS ////
// for realistic mesh 
class SubDomainRectangle : public SubDomain
{
	public:
		const double width = 105.0e-6;
		const double length = 0.5e-3;
		const double radius = 4.3e-6;
		const double rl = 4.3e-6;
		const double rr = 4.3e-6;
		const double tol = 1e-9;
};

// Function for inflow boundary condition for velocity
class VelocityInScalar : public Expression
{
public:
  	double v_max;
  	double v_slip;
  	VelocityInScalar(double max, double slip) : v_max(max), v_slip(slip), Expression() {}

  	void eval(Array<double>& values, const Array<double>& x) const
  	{	
  		double a = 4.3e-6; //x-coordinate of interface / interface_pos_x
    	values[0] = -v_max + (v_max-v_slip)/(a*a)*x[0]*x[0]; //parabolic profile with slip velocity
  	}

};

// Function for inflow boundary condition for velocity
class VelocityIn : public Expression
{
public:
  	double v_max;
  	double v_slip;
  	VelocityIn(double max, double slip) : v_max(max), v_slip(slip), Expression(2) {}

  	void eval(Array<double>& values, const Array<double>& x) const
  	{	
  		double a = 4.3e-6; //x-coordinate of interface / interface_pos_x
    	values[0] = 0.0;
    	values[1] = -v_max + (v_max-v_slip)/(a*a)*x[0]*x[0]; //parabolic profile with slip velocity
  	}

};

// Define subdomain classes
class StokesDomain : public SubDomainRectangle
{
public:
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		if(x[1]>= -this->radius + sqrt(2)*rl || x[1]>= -this->radius + sqrt(2)*rr)
			return x[0] <= this->radius && x[0] >= -this->radius;
		else 
		{ 	
			if(x[0] >= sqrt(2)/2*(this->rl-this->rr))
				return x[1] <= -x[0] + sqrt(2)*this->rr + this->tol && x[1] >= -x[0] - sqrt(2)*this->rr - this->tol;
			else
				return x[1] <= x[0] + sqrt(2)*this->rl + this->tol && x[1] >= x[0] - sqrt(2)*this->rl - this->tol;
		}
	}
};

class DarcyDomain : public SubDomainRectangle
{
public:
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		if(x[1]>= -this->radius + sqrt(2)*rl || x[1]>= -this->radius + sqrt(2)*rr)
			return x[0] >= this->radius || x[0] <= -this->radius;
		else 
		{
			if(x[0] >= sqrt(2)/2*(this->rl-this->rr))
				return x[1] >= -x[0] + sqrt(2)*this->rr && x[1] <= -x[0] - sqrt(2)*this->rr;
			else
				return x[1] >= x[0] + sqrt(2)*this->rl && x[1] <= x[0] - sqrt(2)*this->rl;
		}
	}
};

class Inflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && x[0] <= this->radius && x[0] >= -1*this->radius && near(x[1], this->length);
	}
};

class InflowExceptInterfaceNode : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && x[0] < this->radius && x[0] > -1*this->radius && x[1] > 0;
	}
};

class Outflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && \
			((x[1] <= (-0.5e-3 + this->rl)/sqrt(2) + this->tol && x[1] >= (-0.5e-3 - this->rl)/sqrt(2) - this->tol && near(x[1], -x[0] - sqrt(2)*this->length, this->tol)) \
				|| (x[1] <= (-0.5e-3 + this->rr)/sqrt(2) + this->tol && x[1] >= (-0.5e-3 - this->rr)/sqrt(2) - this->tol && near(x[1], x[0] - sqrt(2)*this->length, this->tol)));
	}
};

class Bottom : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && (near(x[1], -x[0] - sqrt(2)*this->length, this->tol) || near(x[1], x[0] - sqrt(2)*this->length, this->tol));
	}
};

class Top : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[1], this->length);
	}
};

class Right : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		if(x[1]>= -this->width + sqrt(2)*this->width)
			return on_boundary && near(x[0], this->width, this->tol);
		else
			return on_boundary && (near(x[1], -x[0]+sqrt(2)*this->width, this->tol) || near(x[1], -x[0]-sqrt(2)*this->width, this->tol));
	}
};

class Left : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		if(x[1]>= -this->width + sqrt(2)*this->width)
			return on_boundary && near(x[0], -this->width, this->tol);
		else
			return on_boundary && (near(x[1], x[0]+sqrt(2)*this->width, this->tol) || near(x[1], x[0]-sqrt(2)*this->width, this->tol));
	}
};

class Noflow : public SubDomainRectangle
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary; //will be overwritten
	}
};

class Interface : public SubDomainRectangle
{
public:

	bool inside(const Array<double>& x, bool on_boundary) const
	{
		if(x[1]>= -this->radius + sqrt(2)*rl || x[1]>= -this->radius + sqrt(2)*rr)
			return near(x[0], this->radius, this->tol) || near(x[0], -this->radius, this->tol);
		else 
		{
			if(x[0] >= sqrt(2)/2*(this->rl-this->rr))
				return near(x[1], -x[0] + sqrt(2)*this->rr, this->tol) || near(x[1], -x[0] - sqrt(2)*this->rr, this->tol);
			else
				return near(x[1], x[0] + sqrt(2)*this->rl, this->tol) || near(x[1], x[0] - sqrt(2)*this->rl, this->tol);
		}
	}
};

