#include <dolfin.h>
#include "Stokes.h"
#include "Transport.h"

using namespace dolfin;


// Sub domain for inlet
class Inlet : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    double tol = 0.00001;
    return on_boundary && x[2] < 0.1;
  }
};

// Sub domain for outlet
class Outlet : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
     double tol = 0.00001;
     return on_boundary && x[2] > 9.9;
  }
};

// Sub domain for wall
class Wall : public SubDomain
{
   bool inside(const Array<double>& x, bool on_boundary) const
   {
      return on_boundary; //will be overwritten
   }
};

// Function for inflow boundary condition for velocity
class VelocityIn : public Expression
{
public:

  VelocityIn() : Expression(3) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = -0.5*x[0]*x[0]-0.5*x[1]*x[1]+1;
  }

};

// Function for noslip boundary at wall
class Noslip : public Expression
{
public:

  Noslip() : Expression(3) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;
  }

};

int main()
{

  set_log_level(0);

  // Read mesh
  Mesh mesh("../mesh/cylinder.xml");

  /* Stokes Problem ***************************************************/
  /*++++++++++++++++***************************************************/
  // Create subdomains for boundary conditions
  Inlet inlet;
  Outlet outlet;
  Wall wall;
  
  const int dim = mesh.topology().dim();
  mesh.init(dim-1, dim);

  MeshFunction<std::size_t> domains(mesh, dim-1);

  domains.set_all(3);
  wall.mark(domains, 2);
  inlet.mark(domains, 0);
  outlet.mark(domains, 1);

  File domainsfile("results/domains.pvd");
  domainsfile << domains;

  // Create function space and subspaces
  Stokes::FunctionSpace W(mesh);
  SubSpace W0(W, 0);
  SubSpace W1(W, 1);

  // Create functions for boundary conditions
  VelocityIn velocityIn;
  Noslip noslip;
  Constant zero(0);

  // Boundary conditions
  DirichletBC bc0(W0, velocityIn, domains, 0);
  DirichletBC bc1(W0, noslip, domains, 2);
  DirichletBC bc2(W1, zero, domains, 1);

  // Collect boundary conditions
  std::vector<const DirichletBC*> bcs;
  bcs.push_back(&bc0); bcs.push_back(&bc1); bcs.push_back(&bc2);

  // Define variational problem //TODO stabilised grade 1 element are way faster
  Constant f0(0.0, 0.0, 0.0);
  Stokes::BilinearForm a0(W, W);
  Stokes::LinearForm L0(W);
  L0.f = f0; 

  // Compute solution
  Function w(W);
  solve(a0 == L0, w, bcs);
  Function u = w[0];
  Function p = w[1];

  // Save solution in VTK format
  File ufile_pvd("results/velocity.pvd");
  ufile_pvd << u;
  File pfile_pvd("results/pressure.pvd");
  pfile_pvd << p;

  /* Transport Problem ************************************************/
  /*++++++++++++++++***************************************************/
  
  // Create Functions Space
  Transport::FunctionSpace C(mesh);
  
  // Create functions for boundary conditions
  Constant one(1.0);

  // Boundary conditions
  DirichletBC bcc(C, one, inlet);

  // Define variational problem
  Constant f1(0.0);
  Constant beta(0.95);
  Constant D(0.001);
  Function c_prev(C);
  c_prev.interpolate(zero);

  // Parameters for time-stepping
  const double t_stop = 100;
  const double dt = t_stop/20;
  double t = dt;

  Constant timestep(dt);

  // Streamline Upwind Petrov Galerkin
  Transport::BilinearForm a(C, C);
  Transport::LinearForm L(C);
  a.u = u; a.beta = beta; a.D = D; a.dt = timestep;
  L.f = f1; L.u = u; L.beta = beta; L.c_prev = c_prev; L.dt = timestep;

  // Linear system
  boost::shared_ptr<Matrix> A(new Matrix);
  Vector b;

  // Assemble matrix
  assemble(*A, a);
  bcc.apply(*A);

  // LU solver
  LUSolver lu(A);
  lu.parameters["reuse_factorization"] = true;

  // Output file
  File cfile_pvd("results/concentration.pvd");
  
  Function c(C);

  // Time-stepping
  Progress progress("Time-stepping");
  while (t < t_stop)
  {
    // Assemble vector and apply boundary conditions
    assemble(b, L);
    bcc.apply(b);

    // Solve the linear system (re-use the already factorized matrix A)
    lu.solve(*c.vector(), b);

    // Save solution in VTK format
    cfile_pvd << std::make_pair<const Function*, double>(&c, t);

    *c_prev.vector() = *c.vector();

    // Move to next interval
    progress = t / t_stop;
    t += dt;
  }  
  return 0;
}
