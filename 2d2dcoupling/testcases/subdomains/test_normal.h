// SUBDOMAINS FOR TEST 01////

// Define subdomain classes
class StokesDomain : public SubDomain
{
public:
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return x[0] <= 0.5;
	}
};

class DarcyDomain : public SubDomain
{
public:
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return x[0] >= 0.5;
	}
};

class Inflow : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[0], 0);
	}
};

class Outflow : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[0], 1);
	}
};

class Bottom : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[1], 0);
	}
};

class Top : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[1], 1);
	}
};

class Right : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[0], 1);
	}
};

class Left : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[0], 0);
	}
};

class DarcyWall : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && (near(x[1], 0) || near(x[1], 1)) && x[0] >= 0.5;
	}
};

class StokesWall : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && (near(x[1], 0) || near(x[1], 1)) && x[0] <= 0.5;
	}
};

class Interface : public SubDomain
{
public:

	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return near(x[0], 0.5);
	}
};

// Function for inflow boundary condition for velocity
class VelocityIn : public Expression
{
public:
  	double v_max;
  	double v_slip;
  	VelocityIn(double max, double slip) : v_max(max), v_slip(slip), Expression(2) {}

  	void eval(Array<double>& values, const Array<double>& x) const
  	{	
  		double a = 0.5; //x-coordinate of interface / interface_pos_x
    	values[1] = 0.0;
    	values[0] = -v_max + (v_max-v_slip)/(a*a)*(x[1]-0.5)*(x[1]-0.5); //parabolic profile with slip velocity
  	}

};