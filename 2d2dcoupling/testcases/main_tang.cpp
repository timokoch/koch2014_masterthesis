// Stokes Darcy coupled problem in 2D
#include <dolfin.h>
#include "DarcyStokes.h"

using namespace dolfin;

#include "subdomains/test_tang.h"

int main() 
{

	// Debug log level
	list_lu_solver_methods();
	set_log_level(30);
	
	UnitSquareMesh mesh(20, 20, "crossed");
	const int dim = mesh.topology().dim();
	
  	//Initialize mesh
	mesh.init(dim-1, dim);

	File("results/mesh.pvd") << mesh;

	std::cout << "Number of mesh elements: " << mesh.num_cells() << std::endl;
	std::cout << "Number of mesh edges: " << mesh.num_edges() << std::endl;
	std::cout << "Number of mesh vertices: " << mesh.num_vertices() << std::endl;

  		// Initialise subdomain instances and markers
	Inflow inflow; Outflow outflow; DarcyWall darcywall; StokesWall stokeswall; 
	Bottom bottom; Left left; Right right; Top top;
	StokesDomain stokesdomain; DarcyDomain darcydomain; 

  	// Domain indices
  	const int init = 10; //test
	const int stokes = 0; //reeflow
	const int darcy = 1; //porous medium
	// Boundary indices
	const int inlet = 2; //inflow
	const int outlet = 3; //outlet
	const int darcy_wall = 4; //noflow boundary
	const int stokes_wall = 5; //noflow boundary
	// Interior boundaries
	const int interface = 6; // interface between freeflow and porous medium
	const int inside = 7; // inner facets
		
	std::shared_ptr<MeshFunction<std::size_t> > cellDomains (new MeshFunction<std::size_t>(mesh, dim));
	std::shared_ptr<MeshFunction<std::size_t> > intFacetDomains (new MeshFunction<std::size_t>(mesh, dim-1));
	std::shared_ptr<MeshFunction<std::size_t> > extFacetDomains (new MeshFunction<std::size_t>(mesh, dim-1));

	// orientations are stored as auxiliary mesh data
	const int num_facets = mesh.num_facets();
	std::vector<std::size_t>* facet_orientation = NULL;
	mesh.data().create_array("facet_orientation", dim-1);
	facet_orientation = &mesh.data().array("facet_orientation", dim-1);
	facet_orientation->resize(num_facets);
	facet_orientation->assign(num_facets, 1);

	// initialise markers
	intFacetDomains->set_all(init);
	extFacetDomains->set_all(init);

	// Mark all cells either freeflow (stokes) or porous medium (darcy))
	cellDomains->set_all(darcy);
	stokesdomain.mark(*cellDomains, stokes);

	// Iterate over all cells to check if marking was successfull
	std::vector<unsigned int> cells = mesh.cells();
	for (int cell=0; cell < cells.size(); cell++)
	{
		int index = cells[cell];
		int domainType = cellDomains->values()[index];
		if(!(domainType == stokes || domainType == darcy)) 
		{
			std::cout << "not all cells belong to a subdomain!" << std::endl;
			exit(0);
		}
	}

	// Write cell domains file
	File filecellDomains("results/cellDomains.pvd");
	filecellDomains << *cellDomains;

	// Mark exterior boundaries as inlet, outlet and boundary
	darcywall.mark(*extFacetDomains, darcy_wall);
	stokeswall.mark(*extFacetDomains, stokes_wall);
	inflow.mark(*extFacetDomains, inlet);
	outflow.mark(*extFacetDomains, outlet);

	// Mark all interface facets and assign orientations
	for (FacetIterator facet(mesh); !facet.end(); ++facet)	{
		std::size_t index = facet->index();

		if (facet->num_entities(dim) == 1) //if exterior facet
		{
			int cellIdx = facet->entities(dim)[0];
			int domainType = cellDomains->values()[cellIdx];
			int boundaryType = extFacetDomains->values()[index];
			
			if (domainType == stokes) 
			{
				if (boundaryType == inlet)
					(*extFacetDomains)[index] = inlet;
				else if (boundaryType == outlet)
					(*extFacetDomains)[index] = outlet;
				else if (boundaryType == stokes_wall)
					(*extFacetDomains)[index] = stokes_wall;
				else
				{
					std::cout << "error: exterior boundaries of freeflow must be inlet/outlet/stokes_wall" << std::endl;
				}
			}
			else if (domainType == darcy)
			{
				if (boundaryType == inlet)
					(*extFacetDomains)[index] = inlet;
				else if (boundaryType == outlet)
					(*extFacetDomains)[index] = outlet;
				else if (boundaryType == darcy_wall)
					(*extFacetDomains)[index] = darcy_wall;
				else
				{
					std::cout << "error: exterior boundaries of porous medium must be inlet/outlet/darcywall" << std::endl;
				}
			}
		}
		else //if interior facet
		{
			int cell0Idx = facet->entities(dim)[0];
			int cell1Idx = facet->entities(dim)[1];
			int cellType0 = cellDomains->values()[cell0Idx];
			int cellType1 = cellDomains->values()[cell1Idx];
				
			if ((cellType0 == stokes && cellType1 == stokes))
			{
				(*intFacetDomains)[index] = inside;
				(*facet_orientation)[index] = cell0Idx;
			}
			else if (cellType0 == stokes && cellType1 == darcy) 
			{
				(*intFacetDomains)[index] = interface;
				(*facet_orientation)[index] = cell1Idx;
			}
			else if (cellType0 == darcy && cellType1 == stokes)
			{
				(*intFacetDomains)[index] = interface;
				(*facet_orientation)[index] = cell0Idx;
			}
		}
	}


	// Write facet domain file
	File fileextFacet("results/extFacetDomains.pvd");
	fileextFacet << *extFacetDomains;
	File fileintFacet("results/intFacetDomains.pvd");
	fileintFacet << *intFacetDomains;
		
	// Darcy-Stokes fully coupled stationary problem	 
	////////////////////////////////////////////////////////////////////////////////////

	// Create Function Spaces
	DarcyStokes::FunctionSpace W(mesh);
	SubSpace W0(W, 0);
	SubSpace W1(W, 1);

	//Parameter//////////////////////////
	//Permeablity for Darcy domain
	double K_ = 1e-4; 	  
	//Blood viscocity for Stokes domain
	double mu_ = 1e-3; 
	//Stabilization
	double alpha_ = 10;
	//BJS-Coefficient
	double beta_ = 0.1;

	//Output parameters
	std::cout << std::endl;
	std::cout << "Parameters" << std::endl;
	std::cout << "--------------------------------------------------- " << std::endl;	
	std::cout << "      mu = " << mu_ << std::endl;
	std::cout << "       K = " << K_ << std::endl;
	std::cout << "   alpha = " << alpha_ << std::endl;		
	std::cout << "    beta = " << beta_ << std::endl;		
	std::cout << "--------------------------------------------------- " << std::endl;
	std::cout << std::endl;

	////////////////////////////////////////

  	// Define variational problem
	Constant f(0.0, 0.0);
	Constant mu(mu_); Constant K(K_);
	Constant pbar_out(0); Constant pbar_in(0.1);
	Constant alpha(alpha_);  //Velocity stabilization
	Constant beta(beta_);  //Velocity stabilization
	
	//VelocityIn velocityIn(v_max, v_slip);
	Constant vel_zero(0.0, 0.0);

	// Setting v*n is essentiel boundary condition now //tangential part is not affected
	DirichletBC bc0(W0, vel_zero, *extFacetDomains, stokes_wall);
	DirichletBC bc1(W0, vel_zero, *extFacetDomains, darcy_wall);

	// Collect boundary conditions
	std::vector<const DirichletBC*> bcs;
  	bcs.push_back(&bc0); bcs.push_back(&bc1); 

  	DarcyStokes::BilinearForm a(W, W);
  	a.mu = mu; a.K = K; a.alpha = alpha; a.beta = beta;
  	DarcyStokes::LinearForm L(W);
  	L.f = f; L.pbar_out = pbar_out; L.pbar_in = pbar_in;

	// Compute solution
  	Function w(W);
  	Matrix A; Vector b;

  	a.set_interior_facet_domains(intFacetDomains);
  	a.set_cell_domains(cellDomains);
  	a.set_exterior_facet_domains(extFacetDomains);

  	L.set_interior_facet_domains(intFacetDomains);
  	L.set_cell_domains(cellDomains);
  	L.set_exterior_facet_domains(extFacetDomains);

  	assemble(A, a);
  	assemble(b, L);

  	for(std::size_t i = 0; i < bcs.size(); i++)
  		bcs[i]->apply(A, b);

  	std::cout << "Solving coupled Darcy-Stokes system of size <" << A.size(0) << " x " << A.size(1) << ">" << std::endl;

  	if(has_lu_solver_method("mumps"))
  		solve(A, *w.vector(), b, "mumps");
  	else
  	{	
  		std::cout << "Solving without MUMPS solver. May cause memory fault for large problems." << std::endl;
  		solve(A, *w.vector(), b);
  	}

  	Function u = w[0];
  	Function p = w[1];

  	plot(p);
  	plot(u);
  	interactive();

	// Save solution in VTK format
  	File ufile_pvd("results/velocity.pvd");
  	ufile_pvd << u;
  	File pfile_pvd("results/pressure.pvd");
  	pfile_pvd << p;



  	return 0;
}
