cl__1 = 0.1;
cl__2 = 0.03;
Point(1) = {1, 1, 0, cl__1};
Point(2) = {-1, 1, 0, cl__1};
Point(3) = {-1, -1, 0, cl__1};
Point(4) = {1, -1, 0, cl__1};
Point(5) = {-0.1, 1, 0, cl__2};
Point(6) = {0.1, 1, 0, cl__2};

Line(1) = {2, 5};
Line(2) = {5, 6};
Line(3) = {6, 1};

Point(7) = {1, 0.1, 0, cl__2};
Point(8) = {1, -0.1, 0, cl__2};
Line(4) = {2, 3};
Line(5) = {3, 4};
Line(6) = {4, 8};
Line(7) = {7, 1};
Line(8) = {7, 8};
Circle(9) = {5, 1, 8};
Circle(10) = {7, 1, 6};

Line Loop(11) = {1, 9, -6, -5, -4};
Plane Surface(12) = {11};
Line Loop(13) = {10, 3, -7};
Plane Surface(14) = {13};
Line Loop(15) = {2, -10, 8, -9};
Ruled Surface(16) = {15};
