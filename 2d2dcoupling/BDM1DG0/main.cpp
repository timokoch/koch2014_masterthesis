// Stokes Darcy coupled problem in 2D
#include <dolfin.h>
#include "DarcyStokes.h"
#include "Flux.h"
#include "Error.h"
#include "Errorp.h"
#include <iostream>
#include <boost/lexical_cast.hpp>

using namespace dolfin;

//#include "../../subdomains/Roundvessel.h"
#include "../../subdomains/Bifurcation.h"
//#include "../../subdomains/RealRectangle.h"
//#include "../../subdomains/SmallRadius.h"

std::string output_filename(std::string base_name, std::size_t index, std::string suffix_name)
{
	return base_name + "_" + boost::lexical_cast<std::string>( index ) + suffix_name;
}

std::string task_name(std::string base_name, std::size_t index, std::string suffix_name)
{
	std::vector<std::string> ordinal_({"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"});
	return base_name + boost::lexical_cast<std::string>(index) + ordinal_[index] + suffix_name;
}

int main() 
{

	// Debug log level
	list_lu_solver_methods();
	set_log_level(30);

	int refinements = 2;
	
	double error_old;
	double errorp_old;
	double h_old = 0;
	////////////////////////////////////////////////////////////////////////////////

	for(std::size_t refidx_rev=0; refidx_rev<=refinements; refidx_rev++) 
	{	
		std::size_t refidx = refinements - refidx_rev;
		Timer timer(task_name("Solving " , refidx, " refinement step"));
		timer.start();

		Mesh mesh("../../mesh/bifurcation.xml");
		//Mesh mesh("../../mesh/roundvessel.xml");
		//Mesh mesh("../../mesh/realrectangle.xml");
		//Mesh mesh("../../mesh/smallradius.xml");

		const int dim = mesh.topology().dim();
		StokesDomain vessel; 

		if(refidx>0)
			for (int j = 0; j < refidx; ++j)
				mesh = refine(mesh);

  		//Initialize mesh
		mesh.init(dim-1, dim);

		std::cout << std::endl;
    	std::cout << "###################################" << std::endl;
    	std::cout << "Number of mesh refinements: " << refidx << std::endl;
    	std::cout << "###################################" << std::endl;
    	
    	File(output_filename("results/mesh", refidx, "_refinement.xml")) << mesh;

    	std::cout << "Number of mesh elements: " << mesh.num_cells() << std::endl;
    	std::cout << "Number of mesh edges: " << mesh.num_edges() << std::endl;
    	std::cout << "Number of mesh nodes: " << mesh.num_vertices() << std::endl;

  		// Initialise subdomain instances and markers
		Inflow inflow; InflowExceptInterfaceNode inflow_x; Outflow outflow; Noflow noflow; 
		Bottom bottom; Left left; Right right; Top top;

  		// Domain indices
  		const int init = 6; //test
		const int ff = 0; //reeflow
		const int pm = 1; //porous medium
		// Boundary indices
		const int inlet = 2; //inflow
		const int outlet = 3; //outlet
		const int boundary = 4; //noflow boundary
		// Interior boundaries
		const int interface = 5; // interface between freeflow and porous medium
		const int out = 7;
		const int inside = 8;
		
		std::shared_ptr<MeshFunction<std::size_t> > cellDomains (new MeshFunction<std::size_t>(mesh, dim));
		std::shared_ptr<MeshFunction<std::size_t> > intFacetDomains (new MeshFunction<std::size_t>(mesh, dim-1));
		std::shared_ptr<MeshFunction<std::size_t> > extFacetDomains (new MeshFunction<std::size_t>(mesh, dim-1));

		// orientations are stored as auxiliary mesh data
		const int num_facets = mesh.num_facets();
		std::vector<std::size_t>* facet_orientation = NULL;
		mesh.data().create_array("facet_orientation", dim-1);
		facet_orientation = &mesh.data().array("facet_orientation", dim-1);
		facet_orientation->resize(num_facets);
		facet_orientation->assign(num_facets, 1);

		// initialise markers
		intFacetDomains->set_all(init);
		extFacetDomains->set_all(init);

		// Mark all cells either freeflow (ff) or porous medium (pm)
		cellDomains->set_all(pm);
		vessel.mark(*cellDomains, ff);

		// Iterate over all cells to check if marking was successfull
		std::vector<unsigned int> cells = mesh.cells();
		for (int cell=0; cell < cells.size(); cell++)
		{
			int index = cells[cell];
			int domainType = cellDomains->values()[index];
			if(!(domainType == ff || domainType == pm)) 
			{
				std::cout << "not all cells belong to a subdomain!" << std::endl;
				exit(0);
			}
		}

		// Write cell domains file
		File filecellDomains("results/cellDomains.pvd");
		filecellDomains << *cellDomains;

		// Mark exterior boundaries as inlet, outlet and boundary
		bottom.mark(*extFacetDomains, boundary);
		top.mark(*extFacetDomains, boundary);
		left.mark(*extFacetDomains, out);
		right.mark(*extFacetDomains, out);
		inflow.mark(*extFacetDomains, inlet);
		outflow.mark(*extFacetDomains, outlet);

		// Mark all interface facets and assign orientations
		for (FacetIterator facet(mesh); !facet.end(); ++facet)	{
			std::size_t index = facet->index();

			if (facet->num_entities(dim) == 1) //if exterior facet
			{
				int cellIdx = facet->entities(dim)[0];
				int domainType = cellDomains->values()[cellIdx];
				int boundaryType = extFacetDomains->values()[index];
				if (domainType == ff) 

				{
					if (boundaryType == inlet)
						(*extFacetDomains)[index] = inlet;
					else if (boundaryType == outlet)
						(*extFacetDomains)[index] = outlet;
					else
					{
						std::cout << "error: exterior boundaries of freeflow must be inlet or outlet" << std::endl;
						//exit(0);
					}
				}
				else if (domainType == pm)
				{
					if (boundaryType == boundary)
						(*extFacetDomains)[index] = boundary;
					else if (boundaryType == out)
						(*extFacetDomains)[index] = out;
					else
					{
						std::cout << "error: exterior boundaries of porous medium must be boundary or out" << std::endl;
						//exit(0);
					}
				}
			}
			else //if interior facet
			{
				int cell0Idx = facet->entities(dim)[0];
				int cell1Idx = facet->entities(dim)[1];
				int cellType0 = cellDomains->values()[cell0Idx];
				int cellType1 = cellDomains->values()[cell1Idx];
				
				if ((cellType0 == ff && cellType1 == ff))
				{
					(*intFacetDomains)[index] = inside;
					(*facet_orientation)[index] = cell0Idx;
				}
				else if (cellType0 == ff && cellType1 == pm) 
				{
					(*intFacetDomains)[index] = interface;
					(*facet_orientation)[index] = cell1Idx;
				}
				else if (cellType0 == pm && cellType1 == ff)
				{
					(*intFacetDomains)[index] = interface;
					(*facet_orientation)[index] = cell0Idx;
				}
			}
		}


		// Write facet domain file
		File fileextFacet("results/extFacetDomains.pvd");
		fileextFacet << *extFacetDomains;
		File fileintFacet("results/intFacetDomains.pvd");
		fileintFacet << *intFacetDomains;
		

	 	// Darcy-Stokes fully coupled stationary problem
	 	////////////////////////////////////////////////////////////////////////////////////

		// Create Function Spaces
		DarcyStokes::FunctionSpace W(mesh);
		SubSpace W0(W, 0);
		SubSpace W1(W, 1);

		//Parameter///////(5)///////////////////
		////////////////////////////////////////

		//boundary conditions
		//double v_max = 6e-3; 
		//double v_slip = 0;
		
		//Permeablity for Darcy domain
		double k = 6.5e-18; 	  
		//Blood viscocity for Stokes domain
		double mu_ = 2.8e-3; //2e-3; //2.8e-3;  
		 // Coefficients membrane
		double K_m = 2.34e-20;
		double mu_if_ = 1.3e-3;
		double d_m = 0.6e-6;
		double c_m = K_m/(mu_if_*d_m); //4e-10;

	  	/*// for Starling's law
	  	//Included in effective pressure
		double sigma = 0.75;
		double pi = 3600;
		double pi_i = 933;*/

		//Output parameters
		std::cout << std::endl;
		std::cout << "Parameters" << std::endl;
    	std::cout << "--------------------------------------------------- " << std::endl;	
    	std::cout << "mu_blood = " << mu_ << std::endl;
		std::cout << "   mu_if = " << mu_if_ << std::endl;
		std::cout << "       K = " << k << std::endl;
		std::cout << "     K_m = " << K_m << std::endl;
		std::cout << "     d_m = " << d_m << std::endl;
		std::cout << "     L_p = " << c_m << std::endl;		
		std::cout << "--------------------------------------------------- " << std::endl;
		std::cout << std::endl;

		////////////////////////////////////////

	  	// Define variational problem
		Constant f(0.0, 0.0);
		Constant mu(mu_); Constant K(k); Constant C_m(c_m); Constant mu_if(mu_if_);
		Constant pbar_out(-1600); Constant pbar_in(400); Constant p_pmbar(-933);
		Constant alpha(4);  //Velocity stabilization
		
		//VelocityIn velocityIn(v_max, v_slip);
		Constant vel_zero(0.0, 0.0);
		//DirichletBC bc0(W0, velocityIn, inflow);
		// Setting v*n is essentiel boundary condition now //tangential part is not affected
		DirichletBC bc1(W0, vel_zero, *extFacetDomains, boundary);

		// Collect boundary conditions
		std::vector<const DirichletBC*> bcs;
	  	bcs.push_back(&bc1); //bcs.push_back(&bc0); 

	  	DarcyStokes::BilinearForm a(W, W);
		a.mu = mu; a.mu_if = mu_if; a.K = K; a.alpha = alpha; a.C_m = C_m; //a.beta = beta;
		DarcyStokes::LinearForm L(W);
		L.f = f; L.pbar_out = pbar_out; L.pbar_in = pbar_in; L.p_pmbar = p_pmbar; 

		// Compute solution
		Function w(W);
		Matrix A; Vector b;

		a.set_interior_facet_domains(intFacetDomains);
		a.set_cell_domains(cellDomains);
		a.set_exterior_facet_domains(extFacetDomains);
		
		L.set_interior_facet_domains(intFacetDomains);
		L.set_cell_domains(cellDomains);
		L.set_exterior_facet_domains(extFacetDomains);

		assemble(A, a);
		assemble(b, L);
		
		for(std::size_t i = 0; i < bcs.size(); i++)
			bcs[i]->apply(A, b);
		
		std::cout << "Solving coupled Darcy-Stokes system of size <" << A.size(0) << " x " << A.size(1) << ">" << std::endl;

		if(has_lu_solver_method("mumps"))
			solve(A, *w.vector(), b, "mumps");
		else
		{	
			std::cout << "Solving without MUMPS solver. May cause memory fault for large problems." << std::endl;
			solve(A, *w.vector(), b);
		}

		Function u = w[0];
		Function p = w[1];

		//plot(p);
		//interactive();

		// Save solution in VTK format
		File ufile_pvd(output_filename("results/velocity", refidx, "_refinement.pvd"));
		ufile_pvd << u;
		File pfile_pvd(output_filename("results/pressure", refidx, "_refinement.pvd"));
		pfile_pvd << p;

		// // Evaluate flux on interface
		// double integral;
		// Flux::Functional M(mesh);
		// M.u = u;
		// M.set_interior_facet_domains(intFacetDomains);
		// integral = assemble(M);		
		// std::cout << "Integral flux: " << integral << std::endl;

		// // Graphical evaluation of interface flux
		// std::ofstream flux_dat(output_filename("results/flux", refidx, "_refinement.dat"));
		// std::ofstream pressure_dat(output_filename("results/pressure", refidx, "_refinement.dat"));
		// Array<double> flux_values(dim);
		// Array<double> pressure_values(1);
  //   	Array<double> eval_flux_coordinates(dim);
		// int points = 10000;
		// double radius = 4.3e-6;
		// double y_min = -0.5e-3;
		// double y_max = 0.5e-3;
		// double x_min = -105e-6;
		// double x_max = 105e-6;
    	
  //   	//vessel velocity
  //   	eval_flux_coordinates[0] = radius;//50e-6;
		// for (int idx = 0; idx < points; idx++)	
		// {
  //   		eval_flux_coordinates[1] = ((y_max - y_min)/points)*idx + y_min;
  //   		u.eval(flux_values, eval_flux_coordinates);
  // 			flux_dat << eval_flux_coordinates[1] << "; " << flux_values[0] << "\n";
  // 		}

  // 		//vessel pressure
		// eval_flux_coordinates[1] = 0.0; 
		// for (int idx = 0; idx < points; idx++)	
		// {
  //   		eval_flux_coordinates[0] = ((x_max - x_min)/points)*idx + x_min;
  //   		p.eval(pressure_values, eval_flux_coordinates);
  // 			pressure_dat << eval_flux_coordinates[0] << "; " << pressure_values[0] << "\n";
  // 		}

		//round vessel
    // double eval_radius = 2e-3/DOLFIN_PI - 50e-6; 
    // double offset = 2e-3/DOLFIN_PI;
    // for (int idx = 0; idx < points; idx++) 
    // {
    //   eval_flux_coordinates[0] = eval_radius*cos(DOLFIN_PI/(2*points)*idx + DOLFIN_PI) + offset;
    //   if(eval_flux_coordinates[0]>offset) //prevent evaluation from failing
    //     eval_flux_coordinates[0] = offset;
    
    //   eval_flux_coordinates[1] = eval_radius*sin(DOLFIN_PI/(2*points)*idx + DOLFIN_PI) + offset;
    //   if(eval_flux_coordinates[1]>offset) //prevent evaluation from failing
    //     eval_flux_coordinates[1] = offset;

    //    u.eval(flux_values, eval_flux_coordinates);
    //    double n0 = 2e-3/DOLFIN_PI - eval_flux_coordinates[0];
    //    double n1 = 2e-3/DOLFIN_PI - eval_flux_coordinates[1];

    //    flux_dat << DOLFIN_PI/(2*points)*idx + DOLFIN_PI << " " << flux_values[0]*n0/sqrt(n0*n0 + n1*n1) + flux_values[1]*n1/sqrt(n0*n0 + n1*n1) << "\n";
    // }


		// //bifurcation vessel
  //   for (int idx = 0; idx < points; idx++)  
  //   {
  //     if(idx <= points/2)
  //     {
  //       eval_flux_coordinates[0] = 50e-6;
  //       eval_flux_coordinates[1] = ((sqrt(2)-1)*50e-6 - 0.5e-3)/(0.5*points)*idx + 0.5e-3;
  //       u.eval(flux_values, eval_flux_coordinates);
  //       flux_dat << eval_flux_coordinates[1] << " " << flux_values[0] << "\n";
  //     }
  //     else 
  //     { 
  //       double x1 = 50e-6; double y1 = -50e-6 + sqrt(2)*50e-6;
  //       double x2 = (0.5e-3 + 50e-6)/sqrt(2); double y2 = (-0.5e-3 + 50e-6)/sqrt(2);
  //       eval_flux_coordinates[0] = (x2-x1)/(points/2)*idx + 2*x1 - x2;
  //       eval_flux_coordinates[1] = (y2-y1)/(points/2)*idx + 2*y1 - y2;
  //       u.eval(flux_values, eval_flux_coordinates);
  //       flux_dat << eval_flux_coordinates[1] << " " << flux_values[0]/sqrt(2) + flux_values[1]/sqrt(2) << "\n";
  //     }
  //   }
		//flux_dat.close();

		// Timer stop
		set_log_level(0);
		timer.stop();
		set_log_level(30);

		// Error and Rate calculation
		double h = mesh.hmax();
		std::cout << "h_max: " << h << std::endl;
		
		if(refidx == refinements)
		{
			File("u_function.xml") << u;
			File("p_function.xml") << p;
		}

		if(refidx < refinements)
		{
			Mesh mesh_fine(output_filename("results/mesh", refinements, "_refinement.xml"));
			Error::CoefficientSpace_uold VU_fine(mesh_fine);
			Errorp::CoefficientSpace_uold VP_fine(mesh_fine);
			Error::CoefficientSpace_uold VU(mesh);
			Errorp::CoefficientSpace_uold VP(mesh);
			Function u_fine(VU_fine, "u_function.xml");
			Function p_fine(VP_fine, "p_function.xml");
			u.interpolate(u_fine);
			p.interpolate(p_fine);
			Error::Functional MU(mesh_fine, u_fine, u);
			Errorp::Functional MP(mesh_fine, p_fine, p);
			double error = sqrt(assemble(MU));
			double errorp = sqrt(assemble(MP));
			std::cout << "L2-Norm velocity: " << error << std::endl;
			std::cout << "L2-Norm pressure: " << errorp << std::endl;
			if(refidx < refinements-1)
			{
				std::cout << "Experimental order of convergence (velocity): " << (log(error)-log(error_old))/(log(h)-log(h_old)) << std::endl;
      			std::cout << "Experimental order of convergence (pressure): " << (log(errorp)-log(errorp_old))/(log(h)-log(h_old)) << std::endl;
			}
			error_old = error;
			errorp_old = errorp;
			h_old = h;
		}
	}

	return 0;
}