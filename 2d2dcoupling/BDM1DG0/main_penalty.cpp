// Stokes Darcy coupled problem in 2D
#include <dolfin.h>
#include "DarcyStokes.h"
#include "Flux.h"
#include "Error.h"
#include "Errorp.h"
#include <iostream>
#include <boost/lexical_cast.hpp>

using namespace dolfin;

//#include "../../subdomains/Roundvessel.h"
//#include "../../subdomains/Bifurcation.h"
#include "../../subdomains/RealRectangle.h"
//#include "../../subdomains/SmallRadius.h"

std::string output_filename(std::string base_name, std::size_t index, std::string suffix_name)
{
	return base_name + "_" + boost::lexical_cast<std::string>( index ) + suffix_name;
}

std::string task_name(std::string base_name, std::size_t index, std::string suffix_name)
{
	std::vector<std::string> ordinal_({"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"});
	return base_name + boost::lexical_cast<std::string>(index) + suffix_name;
}

int main() 
{

	set_log_level(30);

	int refinements = 100;
	
	double error_old;
	double errorp_old;
	double h_old = 0;
	////////////////////////////////////////////////////////////////////////////////


		//Mesh mesh("../../mesh/bifurcation.xml");
		//Mesh mesh("../../mesh/roundvessel.xml");
		Mesh mesh("../../mesh/realRectangle.xml");
		//Mesh mesh("../../mesh/smallradius.xml");
		const int dim = mesh.topology().dim();
		StokesDomain vessel; 

  		//Initialize mesh
		mesh.init(dim-1, dim);
    	
  		// Initialise subdomain instances and markers
		Inflow inflow; InflowExceptInterfaceNode inflow_x; Outflow outflow; Noflow noflow; 
		Bottom bottom; Left left; Right right; Top top;

  		// Domain indices
  		const int init = 6; //test
		const int ff = 0; //reeflow
		const int pm = 1; //porous medium
		// Boundary indices
		const int inlet = 2; //inflow
		const int outlet = 3; //outlet
		const int boundary = 4; //noflow boundary
		// Interior boundaries
		const int interface = 5; // interface between freeflow and porous medium
		const int out = 7;
		const int inside = 8;
		
		std::shared_ptr<MeshFunction<std::size_t> > cellDomains (new MeshFunction<std::size_t>(mesh, dim));
		std::shared_ptr<MeshFunction<std::size_t> > intFacetDomains (new MeshFunction<std::size_t>(mesh, dim-1));
		std::shared_ptr<MeshFunction<std::size_t> > extFacetDomains (new MeshFunction<std::size_t>(mesh, dim-1));

		// orientations are stored as auxiliary mesh data
		const int num_facets = mesh.num_facets();
		std::vector<std::size_t>* facet_orientation = NULL;
		mesh.data().create_array("facet_orientation", dim-1);
		facet_orientation = &mesh.data().array("facet_orientation", dim-1);
		facet_orientation->resize(num_facets);
		facet_orientation->assign(num_facets, 1);

		// initialise markers
		intFacetDomains->set_all(init);
		extFacetDomains->set_all(init);

		// Mark all cells either freeflow (ff) or porous medium (pm)
		cellDomains->set_all(pm);
		vessel.mark(*cellDomains, ff);

		// Iterate over all cells to check if marking was successfull
		std::vector<unsigned int> cells = mesh.cells();
		for (int cell=0; cell < cells.size(); cell++)
		{
			int index = cells[cell];
			int domainType = cellDomains->values()[index];
			if(!(domainType == ff || domainType == pm)) 
			{
				std::cout << "not all cells belong to a subdomain!" << std::endl;
				exit(0);
			}
		}

		// Write cell domains file
		File filecellDomains("results/cellDomains.pvd");
		filecellDomains << *cellDomains;

		// Mark exterior boundaries as inlet, outlet and boundary
		bottom.mark(*extFacetDomains, boundary);
		top.mark(*extFacetDomains, boundary);
		left.mark(*extFacetDomains, out);
		right.mark(*extFacetDomains, out);
		inflow.mark(*extFacetDomains, inlet);
		outflow.mark(*extFacetDomains, outlet);

		// Mark all interface facets and assign orientations
		for (FacetIterator facet(mesh); !facet.end(); ++facet)	{
			std::size_t index = facet->index();

			if (facet->num_entities(dim) == 1) //if exterior facet
			{
				int cellIdx = facet->entities(dim)[0];
				int domainType = cellDomains->values()[cellIdx];
				int boundaryType = extFacetDomains->values()[index];
				if (domainType == ff) 

				{
					if (boundaryType == inlet)
						(*extFacetDomains)[index] = inlet;
					else if (boundaryType == outlet)
						(*extFacetDomains)[index] = outlet;
					else
					{
						std::cout << "error: exterior boundaries of freeflow must be inlet or outlet" << std::endl;
						//exit(0);
					}
				}
				else if (domainType == pm)
				{
					if (boundaryType == boundary)
						(*extFacetDomains)[index] = boundary;
					else if (boundaryType == out)
						(*extFacetDomains)[index] = out;
					else
					{
						std::cout << "error: exterior boundaries of porous medium must be boundary or out" << std::endl;
						//exit(0);
					}
				}
			}
			else //if interior facet
			{
				int cell0Idx = facet->entities(dim)[0];
				int cell1Idx = facet->entities(dim)[1];
				int cellType0 = cellDomains->values()[cell0Idx];
				int cellType1 = cellDomains->values()[cell1Idx];
				
				if ((cellType0 == ff && cellType1 == ff))
				{
					(*intFacetDomains)[index] = inside;
					(*facet_orientation)[index] = cell0Idx;
				}
				else if (cellType0 == ff && cellType1 == pm) 
				{
					(*intFacetDomains)[index] = interface;
					(*facet_orientation)[index] = cell1Idx;
				}
				else if (cellType0 == pm && cellType1 == ff)
				{
					(*intFacetDomains)[index] = interface;
					(*facet_orientation)[index] = cell0Idx;
				}
			}
		}


		// Write facet domain file
		File fileextFacet("results/extFacetDomains.pvd");
		fileextFacet << *extFacetDomains;
		File fileintFacet("results/intFacetDomains.pvd");
		fileintFacet << *intFacetDomains;
		

	 	// Darcy-Stokes fully coupled stationary problem
	 	////////////////////////////////////////////////////////////////////////////////////

		// Create Function Spaces
		DarcyStokes::FunctionSpace W(mesh);
		SubSpace W0(W, 0);
		SubSpace W1(W, 1);

		//Parameter///////(5)///////////////////
		////////////////////////////////////////

		//boundary conditions
		//double v_max = 6e-3; 
		//double v_slip = 0;
		
		//Permeablity for Darcy domain
		double k = 6.5e-18; 	  
		//Blood viscocity for Stokes domain
		double mu_ = 2.8e-3; //2e-3; //2.8e-3;  
		 // Coefficients membrane
		double K_m = 2.34e-20;
		double mu_if_ = 1.3e-3;
		double d_m = 0.6e-6;
		double c_m = K_m/(mu_if_*d_m); //4e-10;

	  	/*// for Starling's law
	  	//Included in effective pressure
		double sigma = 0.75;
		double pi = 3600;
		double pi_i = 933;*/

		//Output parameters
		std::cout << std::endl;
		std::cout << "Parameters" << std::endl;
    	std::cout << "--------------------------------------------------- " << std::endl;	
    	std::cout << "mu_blood = " << mu_ << std::endl;
		std::cout << "   mu_if = " << mu_if_ << std::endl;
		std::cout << "       K = " << k << std::endl;
		std::cout << "     K_m = " << K_m << std::endl;
		std::cout << "     d_m = " << d_m << std::endl;
		std::cout << "     L_p = " << c_m << std::endl;		
		std::cout << "--------------------------------------------------- " << std::endl;
		std::cout << std::endl;

		////////////////////////////////////////

	  	// Define variational problem
		Constant f(0.0, 0.0);
		Constant mu(mu_); Constant K(k); Constant C_m(c_m); Constant mu_if(mu_if_);
		Constant pbar_out(-1600); Constant pbar_in(400); Constant p_pmbar(-933);
		
		//VelocityIn velocityIn(v_max, v_slip);
		Constant vel_zero(0.0, 0.0);
		//DirichletBC bc0(W0, velocityIn, inflow);
		// Setting v*n is essentiel boundary condition now //tangential part is not affected
		DirichletBC bc1(W0, vel_zero, *extFacetDomains, boundary);

		// Collect boundary conditions
		std::vector<const DirichletBC*> bcs;
	  	bcs.push_back(&bc1); //bcs.push_back(&bc0); 

	  	DarcyStokes::BilinearForm a(W, W);
		a.mu = mu; a.mu_if = mu_if; a.K = K; a.C_m = C_m; //a.beta = beta;
		DarcyStokes::LinearForm L(W);
		L.f = f; L.pbar_out = pbar_out; L.pbar_in = pbar_in; L.p_pmbar = p_pmbar; 

		// Compute solution
		Function w(W);
		Matrix A; Vector b;

		a.set_interior_facet_domains(intFacetDomains);
		a.set_cell_domains(cellDomains);
		a.set_exterior_facet_domains(extFacetDomains);
		
		L.set_interior_facet_domains(intFacetDomains);
		L.set_cell_domains(cellDomains);
		L.set_exterior_facet_domains(extFacetDomains);

		for(std::size_t refidx=0; refidx<=refinements; refidx++) 
		{	
			Timer timer(task_name("Solving:" , refidx, "/1000"));
			timer.start();
			Constant alpha(0.005*refidx);  //Velocity stabilization
			a.alpha = alpha; 

		assemble(A, a);
		assemble(b, L);
		
		for(std::size_t i = 0; i < bcs.size(); i++)
			bcs[i]->apply(A, b);
		
		std::cout << "Solving coupled Darcy-Stokes system of size <" << A.size(0) << " x " << A.size(1) << ">" << std::endl;

		if(has_lu_solver_method("mumps"))
			solve(A, *w.vector(), b, "mumps");
		else
		{	
			std::cout << "Solving without MUMPS solver. May cause memory fault for large problems." << std::endl;
			solve(A, *w.vector(), b);
		}

		Function u = w[0];

		// Graphial evaluation of interface flux
		std::ofstream flux_dat(output_filename("results/flux", refidx, "_penaltyparam.dat"));
		Array<double> flux_values(dim);
    	Array<double> eval_flux_coordinates(dim);
		int points = 10000;
		double radius = 4.3e-6;
		double y_min = -0.5e-3;
		double y_max = 0.5e-3;
		double x_min = -105e-6;
		double x_max = 105e-6;
    	
    	//vessel velocity
    	eval_flux_coordinates[0] = radius;//50e-6;
		for (int idx = 0; idx < points; idx++)	
		{
    		eval_flux_coordinates[1] = ((y_max - y_min)/points)*idx + y_min;
    		u.eval(flux_values, eval_flux_coordinates);
  			flux_dat << eval_flux_coordinates[1] << "; " << flux_values[0] << "\n";
  		}

		flux_dat.close();

		// Timer stop
		set_log_level(0);
		timer.stop();
		set_log_level(30);

	}

	return 0;
}