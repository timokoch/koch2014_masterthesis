#include <dolfin.h>
#include "Transport.h"

using namespace dolfin;

// Sub domain for inlet
class Inlet : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    double tol = 0.0001;
    return on_boundary && x[1] > (1 - tol);
  }
};

// Sub domain for outlet
class Outlet : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
     double tol = 0.0001;
     return on_boundary && x[1] < (-1 + tol);
  }
};

// Sub domain for wall
class Wall : public SubDomain
{
   bool inside(const Array<double>& x, bool on_boundary) const
   {
      return on_boundary; //will be overwritten
   }
};

// Function for inflow boundary condition for velocity
class VelocityIn : public Expression
{
public:
    double v_max;
    VelocityIn(double max) : v_max(max), Expression(2) {}

    void eval(Array<double>& values, const Array<double>& x) const
    { 
      double a = 0.2; //x-coordinate of interface
      values[0] = 0.0;
      values[1] = v_max/(a*a)*(x[0]*x[0] - a*a); //parabolic profile
    }

};

// Function for noslip boundary at wall
class Noslip : public Expression
{
public:

  Noslip() : Expression(2) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
  }
};

class BoundaryConcentration : public Expression
{
public:

  BoundaryConcentration() : Expression() {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    if(x[1]>0.99)
      values[0] = 1.0;
    else
      values[0] = 0.0;
  }
};

int main()
{
  set_log_level(0);

  // Read mesh
  Mesh mesh("../mesh/stokes.xml");

  // Init mesh
  const int dim = mesh.topology().dim();
  mesh.init(dim-1, dim);

   // Create subdomains for boundary conditions
  Inlet inlet;
  Outlet outlet;
  Wall wall;

  // Mesh functions
  boost::shared_ptr<MeshFunction<std::size_t> > exterior_facets_domains (new MeshFunction<std::size_t>(mesh, dim-1));

  // Mark subdomains
  exterior_facets_domains->set_all(3);
  int wallmark = 2;
  int inletmark = 0;
  int outletmark = 1;
  wall.mark(*exterior_facets_domains, wallmark);
  inlet.mark(*exterior_facets_domains, inletmark);
  outlet.mark(*exterior_facets_domains, outletmark);

  File domainsfile("results/exterior_facets_domains.pvd");
  domainsfile << *exterior_facets_domains;

  double v_max = 1;
  VelocityIn velocityIn(v_max);
  Noslip noslip;

  Transport::FunctionSpace C(mesh);
  Transport::BilinearForm a1(C, C);
  Transport::LinearForm L1(C);

  // Define variational problem
  Constant f1(0.0);
  // Stabilisation parameter for DG (default=200) or P1 (default=0.95) method
  Constant beta(8);
  Constant D(0.0);
  Function c_prev(C);
  Function c_0(C);
  BoundaryConcentration boundary_concentration;
  c_prev = boundary_concentration;
  c_0 = c_prev;

  // Output file
  File cfile_pvd("results/concentration.pvd");
  cfile_pvd << std::make_pair<const Function*, double>(&c_prev, 0);

  // Parameters for time-stepping
  const double t_stop = 1;
  const double dt = t_stop/100;
  double t = dt;

  Constant timestep(dt);
  plot(velocityIn, mesh);
  interactive();
  // Streamline Upwind Petrov Galerkin
  a1.dt = timestep; a1.beta = beta; a1.D = D; a1.v = velocityIn;
  L1.f = f1; L1.c_prev = c_prev; L1.dt = timestep; L1.D = D; L1.beta = beta; L1.v = velocityIn;

  // Linear system
  boost::shared_ptr<Matrix> A1(new Matrix);
  Vector b1;

  // Assemble matrix
  assemble(*A1, a1);

  // LU solver
  LUSolver lu(A1);
  lu.parameters["reuse_factorization"] = true;

  Function c(C);

  // Time-stepping
  Progress progress("Time-stepping");
  while (t < t_stop)
  {
    // Assemble vector and apply boundary conditions
    assemble(b1, L1);

    // Solve the linear system (re-use the already factorized matrix A)
    lu.solve(*c.vector(), b1);

    *c_prev.vector() = *c.vector();

    // Save solution in VTK format
    cfile_pvd << std::make_pair<const Function*, double>(&c_prev, t);

    // Move to next interval
    progress = t / t_stop;
    t += dt;
  }  
  system("paraview results/concentration.pvd");
  return 0;
}
