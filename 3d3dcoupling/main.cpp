// Stokes Darcy coupled problem in 2D
#include <dolfin.h>
#include "DarcyStokes.h"
#include "Transport.h"

using namespace dolfin;

// Define subdomain classes
class Vessel : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-4;
		return x[0]*x[0]+x[1]*x[1] <= (2 + tol);
	}
};
class Inflow : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-4;
		return on_boundary && x[0]*x[0]+x[1]*x[1] <= (2+tol) && x[2] > 5;
	}
};
class Outflow : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-4;
		return on_boundary && x[0]*x[0]+x[1]*x[1] <= (2+tol) && x[2] < 5;
	}
};

class Bottom : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-4;
		return on_boundary && x[2] < tol;
	}
};

class Noflow : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary; //will be overwritten
	}
};

// Function for inflow boundary condition for velocity
class VelocityIn : public Expression
{
public:

  VelocityIn() : Expression(3) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = x[0]*x[0] + x[1]*x[1] - 2.1;
  }

};

int main() {
  	// Debug log level
  	set_log_level(0);

  	// Read Mesh and initialise
  	//Mesh mesh("../../mesh/cylindercoupling.xml"); 230000 DOFs, needs more efficient solver using less memory
  	Mesh mesh("../../mesh/cylcouplcoarse.xml");
  	const int dim = mesh.topology().dim();
  	mesh.init(dim-1, dim);

  	// Initialise subdomain instances and markers
  	Vessel vessel; Inflow inflow; Outflow outflow; Noflow noflow; Bottom bottom;

  	// Domain indices
  	const int init = 6; //test
	const int ff = 0; //reeflow
	const int pm = 1; //porous medium
	// Boundary indices
	const int inlet = 2; //inflow
	const int outlet = 3; //outlet
	const int boundary = 4; //noflow boundary
	// Interior boundaries
	const int interface = 5; // interface between freeflow and porous medium

	boost::shared_ptr<MeshFunction<std::size_t> > cellDomains (new MeshFunction<std::size_t>(mesh, dim));
	boost::shared_ptr<MeshFunction<std::size_t> > intFacetDomains (new MeshFunction<std::size_t>(mesh, dim-1));
	boost::shared_ptr<MeshFunction<std::size_t> > extFacetDomains (new MeshFunction<std::size_t>(mesh, dim-1));

	// orientations are stored as auxiliary mesh data
	const int num_facets = mesh.num_facets();
	std::vector<std::size_t>* facet_orientation = NULL;
	mesh.data().create_array("facet_orientation", dim-1);
	facet_orientation = &mesh.data().array("facet_orientation", dim-1);
	facet_orientation->resize(num_facets);
	facet_orientation->assign(num_facets, 1);

	// initialise markers
	intFacetDomains->set_all(init);
	extFacetDomains->set_all(init);

	// Mark all cells either freeflow (ff) or porous medium (pm)
	cellDomains->set_all(pm);
	vessel.mark(*cellDomains, ff);

	// Iterate over all cells to check if marking was successfull
	std::vector<unsigned int> cells = mesh.cells();
	for (CellIterator cell(mesh); !cell.end(); ++cell)
	{
		int index = cell->index();
		int domainType = cellDomains->values()[index];
		if(!(domainType == ff || domainType == pm)) 
		{
			std::cout << "not all cells belong to a subdomain!" << std::endl;
			exit(0);
		}
	}

	// Write cell domains file
	File filecellDomains("results/cellDomains.pvd");
	filecellDomains << *cellDomains;

	// Mark exterior boundaries as inlet, outlet and boundary
	noflow.mark(*extFacetDomains, boundary);
	inflow.mark(*extFacetDomains, inlet);
	outflow.mark(*extFacetDomains, outlet);

	// Mark all interface facets and assign orientations
	for (FacetIterator facet(mesh); !facet.end(); ++facet)	{
		std::size_t index = facet->index();

		if (facet->num_entities(dim) == 1) //if exterior facet
		{
			int cellIdx = facet->entities(dim)[0];
			int domainType = cellDomains->values()[cellIdx];
			int boundaryType = extFacetDomains->values()[index];
			if (domainType == ff) 
			
			{
				if (boundaryType == inlet)
					(*extFacetDomains)[index] = inlet;
				else if (boundaryType == outlet)
					(*extFacetDomains)[index] = outlet;
				else
				{
					std::cout << "error: exterior boundaries of freeflow must be inlet or outlet" << std::endl;
					exit(0);
				}
			}
			else if (domainType == pm)
			{
				if (boundaryType == boundary)
					(*extFacetDomains)[index] = boundary;
				else
				{
					std::cout << "error: exterior boundaries of porous medium must be boundary" << std::endl;
					exit(0);
				}
			}
		}
		else //if interior facet
		{
      		int cell0Idx = facet->entities(dim)[0];
      		int cell1Idx = facet->entities(dim)[1];
			int cellType0 = cellDomains->values()[cell0Idx];
			int cellType1 = cellDomains->values()[cell1Idx];
			
			if ((cellType0 == ff && cellType1 == ff) || (cellType0 == pm && cellType1 == pm))
			{
				continue;
			}
			else if (cellType0 == ff && cellType1 == pm) 
			{
				(*intFacetDomains)[index] = interface;
				(*facet_orientation)[index] = cell1Idx;
			}
			else if (cellType0 == pm && cellType1 == ff)
			{
				(*intFacetDomains)[index] = interface;
				(*facet_orientation)[index] = cell0Idx;
			}
		}
	}

	// Write facet domain file
	File fileextFacet("results/extFacetDomains.pvd");
	fileextFacet << *extFacetDomains;
	File fileintFacet("results/intFacetDomains.pvd");
	fileintFacet << *intFacetDomains;

	// Create Function Spaces
	DarcyStokes::FunctionSpace W(mesh);
	SubSpace W0(W, 0);
	SubSpace W1(W, 1);

	//Functions for boundary conditions
	VelocityIn velocityIn;
	Constant zero(0);
	Constant pbar(0.0); // neumann for outflow

	// Boundary conditions
	DirichletBC bc0(W0, velocityIn, *extFacetDomains, inlet);
	
	// Collect boundary conditions
	std::vector<const DirichletBC*> bcs;
  	bcs.push_back(&bc0); 

  	// Define variational problem
  	Constant f(0.0, 0.0, 0.0);
	Constant mu(1); Constant K(1.0); Constant alpha(0.1);
	DarcyStokes::BilinearForm a(W, W);
	a.mu = mu; a.K = K; a.alpha = alpha;
	DarcyStokes::LinearForm L(W);
	L.f = f; L.pbar = pbar;

	// Set domain decomposition
	a.set_interior_facet_domains(intFacetDomains);
	a.set_cell_domains(cellDomains);
	a.set_exterior_facet_domains(extFacetDomains);
	
	L.set_interior_facet_domains(intFacetDomains);
	L.set_cell_domains(cellDomains);
	L.set_exterior_facet_domains(extFacetDomains);

	// Compute solution
	Function w(W);
	begin("Computing solution.......");
	
	list_krylov_solver_methods();
	list_krylov_solver_preconditioners();
	list_linear_algebra_backends();
	list_linear_solver_methods();

	LinearVariationalProblem problem(a, L, w, bcs);
	LinearVariationalSolver solver(problem);
	solver.solve();


	//Matrix A;
	//Vector b;
	//assemble(A, a);
	//assemble(b, L);

	//for(std::size_t i = 0; i < bcs.size(); i++)
	//	bcs[i]->apply(A, b);

	//solve(A, *w.vector(), b);
	
	end();

	Function u = w[0];
	Function p = w[1];

	// Save solution in VTK format
  	File ufile_pvd("results/velocity.pvd");
  	ufile_pvd << u;
  	File pfile_pvd("results/pressure.pvd");
  	pfile_pvd << p;

  	//Transport equation
  	//////////////////////////////////////////////////////////////

  	std::cout << "Solving transport problem......." << std::endl;

  	// Create Functions Space
 	Transport::FunctionSpace C(mesh);
  
  	// Create functions for boundary conditions
  	Constant one(1.0);

  	// Boundary conditions
  	DirichletBC bcc(C, one, *extFacetDomains , inlet);

  	// Define variational problem
  	Constant f1(0.0);
  	Constant beta(0.95);
  	Constant D(0.001);
  	Function c_prev(C);
  	c_prev.interpolate(zero);

  	// Parameters for time-stepping
  	const double t_stop = 100;
  	const double dt = t_stop/20;
  	double t = dt;

  	Constant timestep(dt);

  	// Streamline Upwind Petrov Galerkin
  	Transport::BilinearForm a1(C, C);
  	Transport::LinearForm L1(C);
  	a1.u = u; a1.beta = beta; a1.D = D; a1.dt = timestep;
 	L1.f = f1; L1.u = u; L1.beta = beta; L1.c_prev = c_prev; L1.dt = timestep;

  	// Linear system
  	boost::shared_ptr<Matrix> A1(new Matrix);
  	Vector b1;

  	// Assemble matrix
  	assemble(*A1, a1);
  	bcc.apply(*A1);

  	// LU solver
  	LUSolver lu(A1);
  	lu.parameters["reuse_factorization"] = true;
  	Function c(C);

  	// Output file
  	File cfile_pvd("results/concentration.pvd");

  	// Time-stepping
  	Progress progress("Time-stepping");
  	while (t < t_stop)
  	{
    	// Assemble vector and apply boundary conditions
    	assemble(b1, L1);
    	bcc.apply(b1);

    	// Solve the linear system (re-use the already factorized matrix A)
    	lu.solve(*c.vector(), b1);

    	// Save solution in VTK format
    	cfile_pvd << std::make_pair<const Function*, double>(&c, t);

    	*c_prev.vector() = *c.vector();

    	// Move to next interval
    	progress = t / t_stop;
    	t += dt;
  	}  
	return 0;
}