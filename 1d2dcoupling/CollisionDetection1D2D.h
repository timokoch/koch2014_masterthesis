std::pair<Point, bool> intersect(Point p1, Point p2, Point p3, Point p4) 
{
  // Store the values for fast access and easy
  // equations-to-code conversion
  double x1 = p1.x(), x2 = p2.x(), x3 = p3.x(), x4 = p4.x();
  double y1 = p1.y(), y2 = p2.y(), y3 = p3.y(), y4 = p4.y();

  double det = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
  // If determinant is zero, there is no intersection
    if (det == 0) return std::make_pair(Point(0.0, 0.0), false);

  // Get the x and y
  double pre = (x1*y2 - y1*x2), post = (x3*y4 - y3*x4);
  double x = ( pre * (x3 - x4) - (x1 - x2) * post ) / det;
  double y = ( pre * (y3 - y4) - (y1 - y2) * post ) / det;

  // Check if the x and y coordinates are within both lines
  if ( x < std::min(x1, x2) - 3.0e-13 || x > std::max(x1, x2) + 3.0e-13 ||
            x < std::min(x3, x4) - 3.0e-13 || x > std::max(x3, x4) + 3.0e-13 )
  {
      return std::make_pair(Point(0.0, 0.0), false);
  }

  if ( y < std::min(y1, y2) - 3.0e-13 || y > std::max(y1, y2) + 3.0e-13||
            y < std::min(y3, y4) - 3.0e-13 || y > std::max(y3, y4) + 3.0e-13)
  {
      return std::make_pair(Point(0.0, 0.0), false);
  }
  // Return the point of intersection
  return std::make_pair(Point(x, y), true);
}

std::pair<std::pair<Point, Point>, bool> collide(std::vector<std::pair<Point, Point> > edges, std::pair<Point, Point>& line)
{
  std::vector<std::pair<Point, bool> > intersections;
  
  Point p0 = line.first;
  Point p1 = line.second;
  Point e0 = edges[0].first;
  Point e1 = edges[0].second;
  
  std::pair<Point, bool> intersection = intersect(p0, p1, e0, e1);
  
  if(intersection.second) // found intersection with edge0
  {
      intersections.push_back(intersection); //save first intersection, then search for more

      e0 = edges[1].first;
      e1 = edges[1].second;
      intersection = intersect(p0, p1, e0, e1);

      if(intersection.second) // found intersection with edge1
      {
        intersections.push_back(intersection); //save second intersection, then return
        return std::make_pair(std::make_pair(intersections[0].first, intersections[1].first), true);
      }
      else
      {
          e0 = edges[2].first;
          e1 = edges[2].second;
          intersection = intersect(p0, p1, e0, e1);

          if(intersection.second) // found intersection with edge2
          {
              intersections.push_back(intersection); //save second intersection, then return
              return std::make_pair(std::make_pair(intersections[0].first, intersections[1].first), true);
          }
          else
          {
              // Intersection is near the beginning or end of the line
              if(intersections[0].first.distance(p0) < intersections[0].first.distance(p1))
                return std::make_pair(std::make_pair(intersections[0].first, p0), true);
              else
                return std::make_pair(std::make_pair(intersections[0].first, p1), true);
          }
      }
  }
  else // search next edge, no intersection with edge0
  {
      e0 = edges[1].first;
      e1 = edges[1].second;
      intersection = intersect(p0, p1, e0, e1);

      if(intersection.second) // found intersection with edge1 (no intersection with edge 0)
      {
          intersections.push_back(intersection); //save intersection and search for more
          e0 = edges[2].first;
          e1 = edges[2].second;
          intersection = intersect(p0, p1, e0, e1);

          if(intersection.second) // found intersection with edge1
          {
              intersections.push_back(intersection); // save second intersection, then return
              return std::make_pair(std::make_pair(intersections[0].first, intersections[1].first), true);
          }
          else //edge1 intersection is close to end or beginning of the line
          {
              if(intersections[0].first.distance(p0) < intersections[0].first.distance(p1))
                return std::make_pair(std::make_pair(intersections[0].first, p0), true);
              else
                return std::make_pair(std::make_pair(intersections[0].first, p1), true);
          }
      }
      else
      {
          e0 = edges[2].first;
          e1 = edges[2].second;
          intersection = intersect(p0, p1, e0, e1);

          if(intersection.second) // found intersection with edge2
          {
              intersections.push_back(intersection);
              if(intersections[0].first.distance(p0) < intersections[0].first.distance(p1))
                return std::make_pair(std::make_pair(intersections[0].first, p0), true);
              else
                return std::make_pair(std::make_pair(intersections[0].first, p1), true);
          }
          else
          {
              return std::make_pair(std::make_pair(Point(0.0, 0.0), Point(0.0, 0.0)), false);
          }
      }
 } 
}
