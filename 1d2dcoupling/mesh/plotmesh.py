from dolfin import *
import sys
mesh_str = str(sys.argv[1]) 
mesh=Mesh(mesh_str)
plot(mesh, interactive=True)
