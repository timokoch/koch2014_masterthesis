// SUBDOMAINS ////
class Inflow : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[0], -0.5e-3);
	}
};

class Outflow : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		return on_boundary && near(x[0], 0.5e-3);
	}
};

class Bottom : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-8;
		return on_boundary && near(x[1], -1e-3, tol);
	}
};

class Top : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-8;
		return on_boundary && near(x[1], 1e-3, tol);
	}
};

class Right : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-8;
		return on_boundary && near(x[0], 1e-3, tol);
	}
};

class Left : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-8;
		return on_boundary && near(x[0], -1e-3, tol);
	}
};

class Vessel : public SubDomain
{
public:

  bool inside(const Array<double>& x, bool on_boundary) const
  {
    
    return near(x[1], -x[0], 1e-7) && x[0] > - 0.5e-3 - 1e-8 && x[0] < 0.5e-3 + 1e-8  ; //vessel
  }
};