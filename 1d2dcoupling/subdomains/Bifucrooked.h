// SUBDOMAINS ////

class Bottom : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-8;
		return on_boundary && near(x[1], -1e-3, tol);
	}
};

class Top : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-8;
		return on_boundary && near(x[1], 1e-3, tol);
	}
};

class Right : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-8;
		return on_boundary && near(x[0], 1e-3, tol);
	}
};

class Left : public SubDomain
{
	bool inside(const Array<double>& x, bool on_boundary) const
	{
		double tol = 1e-8;
		return on_boundary && near(x[0], -1e-3, tol);
	}
};

// Sub domain for inflow ff
class Inflow : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], 0.5e-3); //vessel or bifurcation vessel
  }
};

// Sub domain for outflow ff
class Outflow : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near((x[1]), -0.5e-3/sqrt(2), 1e-8); //bifurcation vessel
  }
};

class Vessel : public SubDomain
{
public:

  bool inside(const Array<double>& x, bool on_boundary) const
  {
    if(x[1]>=0.0)
      return near(x[0], 0.0);
    else 
      return near(x[1], -x[0], 1e-9) || near(x[1], x[0], 1e-9);
  }
};