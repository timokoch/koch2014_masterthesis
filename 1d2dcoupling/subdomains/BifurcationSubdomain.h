class Bottom : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && (near(x[1], x[0]-sqrt(2)*0.5e-3, 1e-9) || near(x[1], -x[0] - sqrt(2)*0.5e-3, 1e-9)); //bifurcation vessel
  }
};

class Top : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], 0.5e-3, 1e-9); //vessel or bifurcation vessel
  }
};

class Right : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    if(x[1]>= -105e-6 + sqrt(2)*105e-6)
      return on_boundary && near(x[0], 105e-6, 1e-9);
    else
      return on_boundary && (near(x[1], -x[0]+sqrt(2)*105e-6, 1e-9) || near(x[1], -x[0]-sqrt(2)*105e-6, 1e-9));
  }
};

class Left : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    if(x[1]>= -105e-6 + sqrt(2)*105e-6)
      return on_boundary && near(x[0], -105e-6, 1e-9);
    else
      return on_boundary && (near(x[1], x[0]+sqrt(2)*105e-6, 1e-9) || near(x[1], x[0]-sqrt(2)*105e-6, 1e-9));
  }
};

// Sub domain for inflow ff
class Inflow : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], 0.5e-3); //vessel or bifurcation vessel
  }
};

// Sub domain for outflow ff
class Outflow : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near((x[1]), -0.5e-3/sqrt(2), 1e-8); //bifurcation vessel
  }
};

class Vessel : public SubDomain
{
public:

  bool inside(const Array<double>& x, bool on_boundary) const
  {
    if(x[1]>=0.0)
      return near(x[0], 0.0);
    else 
      return near(x[1], -x[0], 1e-9) || near(x[1], x[0], 1e-9);
  }
};