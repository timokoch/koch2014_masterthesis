class Bottom : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[0], 2e-3/DOLFIN_PI); //circle vessel
  }
};

class Top : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], 2e-3/DOLFIN_PI); //circle vessel
  }
};

class Right : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && (x[0]-2e-3/DOLFIN_PI)*(x[0]-2e-3/DOLFIN_PI) + (x[1]-2e-3/DOLFIN_PI)*(x[1]-2e-3/DOLFIN_PI) < (2e-3/DOLFIN_PI-105e-6)*(2e-3/DOLFIN_PI-105e-6) + 1e-9; //circle vessel
  }
};

class Left : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && (x[0]-2e-3/DOLFIN_PI)*(x[0]-2e-3/DOLFIN_PI) + (x[1]-2e-3/DOLFIN_PI)*(x[1]-2e-3/DOLFIN_PI) > (2e-3/DOLFIN_PI+105e-6)*(2e-3/DOLFIN_PI+105e-6) - 1e-9;
  }
};

// Sub domain for inflow ff
class Inflow : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], 2e-3/DOLFIN_PI);  //circle vessel
  }
};

// Sub domain for outflow ff
class Outflow : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[0], 2e-3/DOLFIN_PI); //circle vessel
  }
};

class Vessel : public SubDomain
{
public:

  bool inside(const Array<double>& x, bool on_boundary) const
  {
    
    return near((x[0]-2e-3/DOLFIN_PI)*(x[0]-2e-3/DOLFIN_PI) + (x[1]-2e-3/DOLFIN_PI)*(x[1]-2e-3/DOLFIN_PI), (2e-3/DOLFIN_PI)*(2e-3/DOLFIN_PI), 1e-9); //circle vessel
  }
};