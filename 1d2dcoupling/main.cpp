#include <dolfin.h>
#include <boost/lexical_cast.hpp>
#include "Stokes.h"
#include "Darcy.h"
#include "Flux.h"
#include "Error.h"
#include "Error_ff.h"

using namespace dolfin;

#include "subdomains/RectangleSubdomain.h"
//#include "subdomains/CircleSubdomain.h"
//#include "subdomains/BifurcationSubdomain.h"

// Gives back the value of a 2D function as 2D scalar expression
class Coupling : public Expression
{
  public:
  const Function & f;
  SubDomain & vessel;
  Coupling(const Function & u, SubDomain & vessel_subdomain) : f(u), vessel(vessel_subdomain), Expression() {}
        
    void eval(Array<double>& values, const Array<double>& x) const
    {
      double radius = 4.3e-6;
      Array<double> pressure(1);
      Array<double> coordinates_right(2);
      Array<double> coordinates_left(2);
      coordinates_right[1] = x[1];
      coordinates_right[0] = radius;
      coordinates_left[1] = x[1];
      coordinates_left[0] = -radius;

      //circle vessel
      // coordinates_right[0] = x[0] + (2e-3/DOLFIN_PI - x[0])*radius/sqrt((2e-3/DOLFIN_PI - x[0])*(2e-3/DOLFIN_PI - x[0])+(2e-3/DOLFIN_PI - x[1])*(2e-3/DOLFIN_PI - x[1]));
      // coordinates_right[1] = x[1] + (2e-3/DOLFIN_PI - x[1])*radius/sqrt((2e-3/DOLFIN_PI - x[0])*(2e-3/DOLFIN_PI - x[0])+(2e-3/DOLFIN_PI - x[1])*(2e-3/DOLFIN_PI - x[1]));

      // // //bifurcation vessel
      // if(x[1] >= 0.0)
      // {        
      //   coordinates_right[0] = x[0] + 4.3e-6;
      //   coordinates_right[1] = x[1];
      // } 
      // else
      // {
      //   if(x[0] > 0.0) {
      //     coordinates_right[0] = x[0] + 4.3e-6*sqrt(2);
      //     coordinates_right[1] = x[1] + 4.3e-6*sqrt(2); }
      //   else {
      //     coordinates_right[0] = x[0] + 4.3e-6*sqrt(2);
      //     coordinates_right[1] = x[1] - 4.3e-6*sqrt(2); }
      // }

      //circle vessel
      // coordinates_left[0] = x[0] - (2e-3/DOLFIN_PI - x[0])*radius/sqrt((2e-3/DOLFIN_PI - x[0])*(2e-3/DOLFIN_PI - x[0])+(2e-3/DOLFIN_PI - x[1])*(2e-3/DOLFIN_PI - x[1]));
      // coordinates_left[1] = x[1] - (2e-3/DOLFIN_PI - x[1])*radius/sqrt((2e-3/DOLFIN_PI - x[0])*(2e-3/DOLFIN_PI - x[0])+(2e-3/DOLFIN_PI - x[1])*(2e-3/DOLFIN_PI - x[1]));
        
      //bifurcation vessel
      // if(x[1] >= 0.0)
      // {        
      //   coordinates_left[0] = x[0] - 4.3e-6;
      //   coordinates_left[1] = x[1];
      // } 
      // else
      // {
      //   if(x[0] > 0.0) {
      //     coordinates_left[0] = x[0] - 4.3e-6*sqrt(2);
      //     coordinates_left[1] = x[1] - 4.3e-6*sqrt(2); }
      //   else {
      //     coordinates_left[0] = x[0] + 4.3e-6*sqrt(2);
      //     coordinates_left[1] = x[1] - 4.3e-6*sqrt(2); }
      // }


      f.eval(pressure, coordinates_left);
      values[0] = 0.5*pressure[0];
      f.eval(pressure, coordinates_right);
      values[0] += 0.5*pressure[0];
    }
};

// Initialize functions with a constant exression
class Initialize_scalar_function : public Expression
{
public:
  double value;
  Initialize_scalar_function(double x) : value(x), Expression() {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = value;
  }

};

// Initialize functions with a constant exression
class Initialize_scalar_function_linear : public Expression
{
public:
  Initialize_scalar_function_linear() : Expression() {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 2e6*x[1] - 600;
  }

};

std::vector<std::pair<Point, double> > calculate_integration_points(Mesh mesh, int n)
{
  std::vector<std::pair<Point, double> > integration_points;
  std::vector<double> x;
  std::vector<double> alpha;
  
  x.resize(n);
  alpha.resize(n);
  
  switch(n)
  {
    case 1:
      x[0] = 0.0;
      alpha[0] = 2.0;
      break;
    case 2: 
      x[0] = -sqrt(1.0/3.0);
      x[1] = sqrt(1.0/3.0);
      alpha[0] = 1.0;
      alpha[1] = 1.0;
      break;
    case 3:
      x[0] = -sqrt(3.0/5.0);
      x[1] = 0.0;
      x[2] = sqrt(3.0/5.0);
      alpha[0] = 5.0/9.0;
      alpha[1] = 8.0/9.0;
      alpha[2] = 5.0/9.0;
      break;
    case 4: 
      x[0] = -sqrt(3.0/7.0 + 2.0/7.0*sqrt(6.0/5.0));
      x[1] = -sqrt(3.0/7.0 - 2.0/7.0*sqrt(6.0/5.0));
      x[2] = sqrt(3.0/7.0 - 2.0/7.0*sqrt(6.0/5.0));
      x[3] = sqrt(3.0/7.0 + 2.0/7.0*sqrt(6.0/5.0));
      alpha[0] = (18.0-sqrt(30.0))/36.0;
      alpha[1] = (18.0+sqrt(30.0))/36.0;
      alpha[2] = (18.0+sqrt(30.0))/36.0;
      alpha[3] = (18.0-sqrt(30.0))/36.0;
      break;
    default:
      std::cout << "Integration order not implemented!";
      exit(0);
  }

  std::cout << std::endl << "Chosen Gaussian integration with "<< n << " integration points on interval [-1, 1]";
  for (int i = 0; i < n; ++i)
  {
    std::cout << " (x[" << i << "] = " << x[i] << ", alpha[" << i << "] = " << alpha[i] << ");";
  }
  std::cout<<std::endl;

  // for every intersection pair calculate all integration points
  // Mark all interface facets and assign orientations
  for (CellIterator cell(mesh); !cell.end(); ++cell)  {
    Vertex va(mesh, cell->entities(0)[0]);
    Vertex vb(mesh, cell->entities(0)[1]);

    Point a = va.point();
    Point b = vb.point();
    double distance = a.distance(b);
    double x_a = a.x();
    double y_a = a.y();
    double x_b = b.x();
    double y_b = b.y();

    // If intersection coincides with a mesh node, false intersection can be generated. Jump those.
    if(distance < 1e-14)
    {     std::cout << "jumped currupt intersection pair" << std::endl;
            continue;}

    for (int j = 0; j < x.size(); ++j)
    {
      // Find intersection of circle around a with radius distance_c and line through a, b
      double distance_c = x[j]*distance/2 + distance/2;
      double x_c, y_c;
      if(x_b == x_a) //line is vertical
      {
        x_c = x_a; 
        y_c = y_a + distance_c;
      }
      else if(y_b == y_a) //line is horizontal
      {
        y_c = y_a;
        x_c = x_a + distance_c;
      }
      else //line not vertical or horizontal
      {
        x_c = x_a + distance_c/sqrt(1.0 + (y_b - y_a)*(y_b - y_a)/((x_b - x_a)*(x_b - x_a)));
        y_c = (y_b - y_a)/(x_b - x_a)*(x_c - x_a) + y_a;
      }

      Point c(x_c, y_c, 0);
      
      // If point not between a and b change signs
      if(b.distance(c) > distance)
      {
        if(x_b == x_a) //line is vertical
        {
          x_c = x_a;
          y_c = y_a - distance_c;
        }
        else if (y_b == y_a) //line is horizontal
        {
          y_c = y_a;
          x_c = x_a - distance_c;
        }
        else
        {
          x_c = x_a - distance_c/sqrt(1.0 + (y_b - y_a)*(y_b - y_a)/((x_b - x_a)*(x_b - x_a)));
          y_c = (y_b - y_a)/(x_b - x_a)*(x_c - x_a) + y_a;
        }
        c[0] = x_c; c[1] = y_c;
      }

      integration_points.push_back(std::make_pair(c, distance/2*alpha[j]));
    }
  }
  return integration_points;
}

std::string output_filename(std::string base_name, std::size_t index, std::string suffix_name)
{
  return base_name + "_" + boost::lexical_cast<std::string>( index ) + suffix_name;
}

std::string task_name(std::string base_name, std::size_t index, std::string suffix_name)
{
  std::vector<std::string> ordinal_({"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"});
  return base_name + boost::lexical_cast<std::string>(index) + ordinal_[index] + suffix_name;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////MAINMAIN////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
  list_lu_solver_methods();
  parameters("lu_solver")["reuse_factorization"] = true;
  parameters("lu_solver")["same_nonzero_pattern"] = true;

  set_log_level(30);

  int refinements = 7;
  double error_old;
  double error_ff_old;
  double h_old = 0;
  double h_ff_old = 0;

  for(std::size_t refidx_rev=0; refidx_rev<=refinements; refidx_rev++) 
  {   

    std::size_t refidx = refinements - refidx_rev;

    Timer timer(task_name("Solving " , refidx, " refinement step"));
    timer.start();
    // The mesh
    RectangleMesh bmesh(-1.05e-4, -0.5e-3, 1.05e-4, 0.5e-3, 2*pow(2, refidx+1), 4*pow(2, refidx+1), "crossed"); 
    Mesh mesh = bmesh; 
    //Mesh mesh("./mesh/circletissue.xml");
    //Mesh mesh("mesh/bifurcationtissue.xml");
    
    const int dim = mesh.topology().dim();

    // The interval mesh (1D Stokes domain in 2D world)
    Mesh mesh_ff("mesh/vesselmesh.xml"); 
    //Mesh mesh_ff("./mesh/circlevessel.xml"); 
    //Mesh mesh_ff("mesh/bifurcationvessel.xml"); 
    
    const int dim_ff = mesh_ff.topology().dim();
    /*
    if(refidx>0) 
      for (int numrefine = 0; numrefine < refidx; ++numrefine)
        mesh = refine(mesh); */   

    if(refidx>0)
      for (int numrefine = 0; numrefine < refidx; ++numrefine)
        mesh_ff = refine(mesh_ff);

    mesh.init();
    mesh_ff.init();

    std::cout << std::endl;
    std::cout << "###################################" << std::endl;
    std::cout << "Number of mesh refinements: " << refidx << std::endl;
    std::cout << "###################################" << std::endl;

    // Initialize subdomains
    Left left; Right right; Top top; Bottom bottom;
    Inflow inflow; Outflow outflow;
    Vessel vessel; 

    // Create mesh functions
    std::shared_ptr<MeshFunction<std::size_t> > vertex_domains_ff(new MeshFunction<std::size_t> (mesh_ff, dim_ff-1));
    std::shared_ptr<MeshFunction<std::size_t> > facet_domains(new MeshFunction<std::size_t> (mesh, dim-1));

    File(output_filename("results/mesh", refidx, "_refinement.pvd")) << mesh;
    File(output_filename("results/mesh_ff", refidx, "_refinement.pvd")) << mesh_ff;

    std::cout << "Number of mesh elements (ff): " << mesh_ff.num_cells() << std::endl;
    std::cout << "Number of mesh edges (ff): " << mesh_ff.num_edges() << std::endl;
    std::cout << "Number of mesh nodes (ff): " << mesh_ff.num_vertices() << std::endl;
    std::cout << "Number of mesh elements (pm): " << mesh.num_cells() << std::endl;
    std::cout << "Number of mesh edges (pm): " << mesh.num_edges() << std::endl;
    std::cout << "Number of mesh nodes (pm): " << mesh.num_vertices() << std::endl;

    // integration points for line source
    int gauss_degree = 2; 
    std::vector<std::pair<Point, double> > integration_points = calculate_integration_points(mesh_ff, gauss_degree);
    //for (int ipidx = 0; ipidx < integration_points.size(); ++ipidx)
    //  std::cout << "Integration point " << ipidx << ": " << integration_points[ipidx].first << " with weight:  " << integration_points[ipidx].second << std::endl;
    std::cout << "Number of integration points in total: " << integration_points.size() << std::endl; 

    facet_domains->set_all(0);
    left.mark(*facet_domains, 1);
    right.mark(*facet_domains, 1);

    File("results/facet_domains.pvd") << *facet_domains;

    vertex_domains_ff->set_all(2);
    int in = 0; int out = 1; 
    //vessel.mark(*vertex_domains_ff, 18);
    inflow.mark(*vertex_domains_ff, in);
    outflow.mark(*vertex_domains_ff, out);

    File("results/vertex_domains_ff.pvd") << *vertex_domains_ff;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create function space and subspaces
    Stokes::FunctionSpace PFF(mesh_ff);
    Darcy::FunctionSpace PPM(mesh);

    // Create functions for boundary conditions
    double p_boundary = -933;
    Constant pressure(p_boundary);

    double p_out = -1600;
    double p_in = 400;
    Constant pressure_in(p_in);
    Constant pressure_out(p_out);

    // Coefficients vessel
    double mu_blood = 2.8e-3; //2e-3; //2.8e-3;
    double gamma = 2;
    double radius = 4.3e-6;
     // Coefficients tissue
    double K_t = 6.5e-18;
    double mu_if = 1.3e-3;
    // Coefficients membrane
    double K_m = 2.34e-20;
    double d_m = 0.6e-6;
    double L_p = K_m/d_m/mu_if; //1.5e-9; //2.5e-12; 

    //Relaxation parameter
    double theta = 0.26;
    double theta_ff = theta;//0.26;

    // Coefficient for Stokes
    double c_stokes = radius*radius*radius/(2*L_p*mu_blood*(2+gamma));

    //Output parameters
    std::cout << std::endl;
    std::cout << "Parameters" << std::endl;
    std::cout << "--------------------------------------------------- " << std::endl;
    std::cout << "mu_blood = " << mu_blood << std::endl;
    std::cout << "   mu_if = " << mu_if << std::endl;
    std::cout << "       K = " << K_t << std::endl;
    std::cout << "     K_m = " << K_m << std::endl;
    std::cout << "     d_m = " << d_m << std::endl;
    std::cout << "     L_p = " << L_p << std::endl;
    std::cout << "   gamma = " << gamma << std::endl;
    std::cout << "  radius = " << radius << std::endl;
    std::cout << "   theta = " << theta << std::endl;
    std::cout << "--------------------------------------------------- " << std::endl;
    std::cout << std::endl;

    Constant C_stokes(c_stokes);
    Constant K(K_t); Constant Mu_if(mu_if); Constant Mu_blood(mu_blood);
    Constant g(0.0); Constant L_p_1(1/L_p);

    // Define variational problems //////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Darcy
    DirichletBC bcleft(PPM, pressure, left);
    DirichletBC bcright(PPM, pressure, right);
   
    // Collect boundary conditions
    std::vector<const DirichletBC*> bcd;
    bcd.push_back(&bcleft); 
    bcd.push_back(&bcright); 
    
    Darcy::BilinearForm a(PPM, PPM);
    a.K = K; a.Mu_if = Mu_if; 

    Darcy::LinearForm L(PPM);
    L.g = g; 

    a.set_exterior_facet_domains(facet_domains);
    L.set_exterior_facet_domains(facet_domains);

    // Assemle right hand side
    std::shared_ptr<Matrix> A(new Matrix());
    std::shared_ptr<Vector> b(new Vector());
    assemble(*A, a);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Stokes
    DirichletBC bc0(PFF, pressure_in, *vertex_domains_ff, in);
    DirichletBC bc1(PFF, pressure_out, *vertex_domains_ff, out);

    // Collect boundary conditions
    std::vector<const DirichletBC*> bcs;
    bcs.push_back(&bc0); bcs.push_back(&bc1); 

    Stokes::BilinearForm a1(PFF, PFF);
    a1.C_stokes = C_stokes; 

    Stokes::LinearForm L1(PFF);

    a1.set_exterior_facet_domains(vertex_domains_ff);
    L1.set_exterior_facet_domains(vertex_domains_ff);

    // Assemble 
    std::shared_ptr<Matrix> A1(new Matrix());
    std::shared_ptr<Vector> b1(new Vector());
    assemble(*A1, a1);
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Output matrix sizes
    std::cout << std::endl;
    std::cout << "Solving linear Darcy system of size <" << A->size(0) << " x " << A->size(1) << ">" << std::endl;
    std::cout << "Solving linear Stokes system of size <" << A1->size(0) << " x " << A1->size(1) << ">" << std::endl;
    std::cout << "Overall degrees of freedom <" << A->size(0)+A1->size(0) << ">" << std::endl;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create solution functions
    Function p_ff(PFF);
    Function p_ff_old(PFF);
    Function p_pm(PPM);
    Function p_pm_old(PPM);
    p_pm_old = Initialize_scalar_function(-933.0);
    p_ff_old = Initialize_scalar_function_linear();
    p_ff.rename("p_ff", "vessel pressure");
    p_pm.rename("p_pm", "tissue pressure");

    File p_ff_pvd(output_filename("results/pressure_ff", refidx, "_refinement.pvd"));
    File p_pm_pvd(output_filename("results/pressure_pm", refidx, "_refinement.pvd"));

    // For point sources
    Array<double> source_values_ff(1);
    Array<double> source_values_ff_2(1);
    Array<double> source_values_pm_right(1);
    Array<double> source_values_pm_left(1);
    Array<double> source_coordinates(dim);
    Array<double> coordinates_right(dim);
    Array<double> coordinates_left(dim);


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Iterative Algorithm
    long double pm_error = 1.0;
    long double ff_error = 1.0;
    double tol = 1e-10; 
    int iterations = 0;
    int max_iter = 5000;
    
    std::cout << "starting iterative algorithm with parameters: ";
    std::cout << "tolerance="<< tol;
    std::cout << "; max_iter="<< max_iter << std::endl << std::endl;
   
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///////////////////   ALORITHM       ///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //Solvers
    LUSolver darcy_solver(A);
    LUSolver stokes_solver(A1);
    darcy_solver.parameters["reuse_factorization"] = true;
    stokes_solver.parameters["reuse_factorization"] = true;

    double source_integral = 0;

    while (pm_error > tol) 
    {
      Timer timeriter("iteration step");
      timeriter.start();

      ++iterations;
      // If it doesn't converge throw error
      if (iterations >= max_iter) 
      { 
        std::cout << "Iterative method didn't converge after " << max_iter << " steps!" << std::endl; 
        exit(0); 
      }

      Timer timer_1("Solve Darcy System");
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////  DARCY  ////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////
      //Assemble right hand side of Darcy system
      assemble(*b, L);
      Timer timer_2("Calculate source terms");

      // Reset source integral to zero
      source_integral = 0;
      
      // Create and apply point sources (numerical integration)
      for (int i = 0; i < integration_points.size(); ++i)
      {
        source_coordinates[0] = integration_points[i].first.x();
        source_coordinates[1] = integration_points[i].first.y();
        p_ff_old.eval(source_values_ff, source_coordinates);

        //vessel
        coordinates_right[0] = source_coordinates[0] + radius;
        coordinates_right[1] = source_coordinates[1];
        
        //circle vessel
        // coordinates_right[0] = source_coordinates[0] + (2e-3/DOLFIN_PI - source_coordinates[0])*radius/sqrt((2e-3/DOLFIN_PI - source_coordinates[0])*(2e-3/DOLFIN_PI - source_coordinates[0])+(2e-3/DOLFIN_PI - source_coordinates[1])*(2e-3/DOLFIN_PI - source_coordinates[1]));
        // coordinates_right[1] = source_coordinates[1] + (2e-3/DOLFIN_PI - source_coordinates[1])*radius/sqrt((2e-3/DOLFIN_PI - source_coordinates[0])*(2e-3/DOLFIN_PI - source_coordinates[0])+(2e-3/DOLFIN_PI - source_coordinates[1])*(2e-3/DOLFIN_PI - source_coordinates[1]));
        
        //bifurcation vessel
        // if(source_coordinates[1] >= 0.0)
        // {        
        //   coordinates_right[0] = source_coordinates[0] + 4.3e-6;
        //   coordinates_right[1] = source_coordinates[1];
        // } 
        // else
        // {
        //   if(source_coordinates[0] > 0.0) {
        //     coordinates_right[0] = source_coordinates[0] + 4.3e-6*sqrt(2);
        //     coordinates_right[1] = source_coordinates[1] + 4.3e-6*sqrt(2); }
        //   else {
        //     coordinates_right[0] = source_coordinates[0] + 4.3e-6*sqrt(2);
        //     coordinates_right[1] = source_coordinates[1] - 4.3e-6*sqrt(2); }
        // }

        p_pm_old.eval(source_values_pm_right, coordinates_right);
        
        //vessel
        coordinates_left[0] = source_coordinates[0] - radius;
        coordinates_left[1] = source_coordinates[1];
        
        //circle vessel
        // coordinates_left[0] = source_coordinates[0] - (2e-3/DOLFIN_PI - source_coordinates[0])*radius/sqrt((2e-3/DOLFIN_PI - source_coordinates[0])*(2e-3/DOLFIN_PI - source_coordinates[0])+(2e-3/DOLFIN_PI - source_coordinates[1])*(2e-3/DOLFIN_PI - source_coordinates[1]));
        // coordinates_left[1] = source_coordinates[1] - (2e-3/DOLFIN_PI - source_coordinates[1])*radius/sqrt((2e-3/DOLFIN_PI - source_coordinates[0])*(2e-3/DOLFIN_PI - source_coordinates[0])+(2e-3/DOLFIN_PI - source_coordinates[1])*(2e-3/DOLFIN_PI - source_coordinates[1]));
        
        //bifurcation vessel
        // if(source_coordinates[1] >= 0.0)
        // {        
        //   coordinates_left[0] = source_coordinates[0] - 4.3e-6;
        //   coordinates_left[1] = source_coordinates[1];
        // } 
        // else
        // {
        //   if(source_coordinates[0] > 0.0) {
        //     coordinates_left[0] = source_coordinates[0] - 4.3e-6*sqrt(2);
        //     coordinates_left[1] = source_coordinates[1] - 4.3e-6*sqrt(2); }
        //   else {
        //     coordinates_left[0] = source_coordinates[0] + 4.3e-6*sqrt(2);
        //     coordinates_left[1] = source_coordinates[1] - 4.3e-6*sqrt(2); }
        // }

        p_pm_old.eval(source_values_pm_left, coordinates_left);

        double magnitude = integration_points[i].second*(source_values_ff[0]-source_values_pm_right[0])*L_p;
        magnitude += integration_points[i].second*(source_values_ff[0]-source_values_pm_left[0])*L_p;

        PointSource f(PPM, integration_points[i].first, magnitude);        
        f.apply(*b);

        source_integral += magnitude;
      }
      set_log_level(0);
      timer_2.stop();
      set_log_level(30);

      // Boundary conditions for continuous Galerkin
      for(std::size_t bcidx = 0; bcidx < bcd.size(); bcidx++)
        bcd[bcidx]->apply(*A, *b);

      // Solve 2D Darcy variational problem 
      darcy_solver.solve(*p_pm.vector(), *b);

      set_log_level(0);
      timer_1.stop();
      set_log_level(30);
      
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////  STOKES  ///////////////////////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////
      Timer timer_3("Solve Stokes System");

      Coupling p_pm_to_stokes(p_pm, vessel);
      //parameters["allow_extrapolation"] = true;
      L1.p_pm = p_pm_to_stokes;
      assemble(*b1, L1);
      for(std::size_t bcidx = 0; bcidx < bcs.size(); bcidx++)
        bcs[bcidx]->apply(*A1, *b1);
      
      // Solve 1D Stokes variational problem
      stokes_solver.solve(*p_ff.vector(), *b1);

      set_log_level(0);
      timer_3.stop();
      set_log_level(30);

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////  ERROR CALCULATION  ////////////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////

      // Error calculation L2 norm doesnt work (precision)
      // -> Error calculation in l2-norm
      // -> max error
      
      Function error_function_P(PPM);
      error_function_P = p_pm;
      *error_function_P.vector() -= *p_pm_old.vector();
      //pm_error = error_function_P.vector()->norm("l2");
      error_function_P.vector()->abs();
      pm_error = error_function_P.vector()->max();
      Function error_function_P_FF(PFF);
      error_function_P_FF = p_ff;
      *error_function_P_FF.vector() -= *p_ff_old.vector();
      //ff_error = error_function_P_FF.vector()->norm("l2");
      error_function_P_FF.vector()->abs();
      ff_error = error_function_P_FF.vector()->max();

      // Print number of iterations and error
      std::cout << "Iteration " << iterations << " done with error (pm) = " << pm_error << std::endl;
      std::cout << "Iteration " << iterations << " done with error (ff) = " << ff_error << std::endl;
    
      // p(k) becomes p(k+1) with relaxation
      p_pm = p_pm*(1-theta);
      p_pm_old = p_pm_old*theta;
      p_pm_old = p_pm_old + p_pm; 

      // p(k) becomes p(k+1) with relaxation
      p_ff = p_ff*(1-theta_ff);
      p_ff_old = p_ff_old*theta_ff;
      p_ff_old = p_ff_old + p_ff; 

      set_log_level(0);
      timeriter.stop();
      set_log_level(30);

    }
    
    Timer timer_4("Post-Processing");

    p_pm = p_pm/(1-theta); //Reverse calculation from above for evaluation!!!!
    p_ff = p_ff/(1-theta_ff); //Reverse calculation from above!!!!
    
    // Output pvd
    p_ff_pvd << p_ff;
    p_pm_pvd << p_pm;

    //Source integral
    std::cout << "Source integral: " << source_integral << std::endl;

    // // Graphical evaluation of interface flux
    Flux::FunctionSpace VPM(mesh);
    Flux::BilinearForm a2(VPM, VPM);
    Flux::LinearForm L2(VPM);
    Function vel(VPM);
    Constant K_t_(K_t);
    Constant mu_if_(mu_if);
    L2.K = K_t_; L2.mu = mu_if_; L2.p = p_pm;
    Matrix A2; Vector b2;
    assemble(A2, a2); 
    assemble(b2, L2);
    solve(A2, *vel.vector(), b2);

    File("results/vel.pvd") << vel;

    std::ofstream flux_dat(output_filename("results/flux", refidx, "_refinement.dat"));
    Array<double> flux_values(2);
    Array<double> eval_flux_coordinates(2);
    int num_points = 10000;
    double min = -0.5e-3;
    double max = 0.5e-3;

    //vessel
    eval_flux_coordinates[0] = radius; //50e-6;
    for (int idx = 0; idx < num_points; idx++)  
    {
        eval_flux_coordinates[1] = ((max - min)/num_points)*idx + min;
        vel.eval(flux_values, eval_flux_coordinates);
        flux_dat << eval_flux_coordinates[1] << " " << flux_values[0] << "\n";
    }

    // //round vessel
    // double eval_radius = 2e-3/DOLFIN_PI + 50e-6; 
    // double offset = 2e-3/DOLFIN_PI;
    // for (int idx = 0; idx < num_points; idx++) 
    // {
    //   eval_flux_coordinates[0] = eval_radius*cos(DOLFIN_PI/(2*num_points)*idx + DOLFIN_PI) + offset;
    //   if(eval_flux_coordinates[0]>offset) //prevent evaluation from failing
    //     eval_flux_coordinates[0] = offset;
    
    //   eval_flux_coordinates[1] = eval_radius*sin(DOLFIN_PI/(2*num_points)*idx + DOLFIN_PI) + offset;
    //   if(eval_flux_coordinates[1]>offset) //prevent evaluation from failing
    //     eval_flux_coordinates[1] = offset;

    //    vel.eval(flux_values, eval_flux_coordinates);
    //    double n0 = 2e-3/DOLFIN_PI - eval_flux_coordinates[0];
    //    double n1 = 2e-3/DOLFIN_PI - eval_flux_coordinates[1];

    //    flux_dat << DOLFIN_PI/(2*num_points)*idx + DOLFIN_PI << " " << flux_values[0]*n0/sqrt(n0*n0 + n1*n1) + flux_values[1]*n1/sqrt(n0*n0 + n1*n1) << "\n";
    // }

    // //bifurcation vessel
    // for (int idx = 0; idx < num_points; idx++)  
    // {
    //   if(idx <= num_points/2)
    //   {
    //     eval_flux_coordinates[0] = 50e-6;
    //     eval_flux_coordinates[1] = ((sqrt(2)-1)*50e-6 - 0.5e-3)/(0.5*num_points)*idx + 0.5e-3;
    //     vel.eval(flux_values, eval_flux_coordinates);
    //     flux_dat << eval_flux_coordinates[1] << " " << flux_values[0] << "\n";
    //   }
    //   else 
    //   { 
    //     double x1 = 50e-6; double y1 = -50e-6 + sqrt(2)*50e-6;
    //     double x2 = (0.5e-3 + 50e-6)/sqrt(2); double y2 = (-0.5e-3 + 50e-6)/sqrt(2);
    //     eval_flux_coordinates[0] = (x2-x1)/(num_points/2)*idx + 2*x1 - x2;
    //     eval_flux_coordinates[1] = (y2-y1)/(num_points/2)*idx + 2*y1 - y2;
    //     vel.eval(flux_values, eval_flux_coordinates);
    //     flux_dat << eval_flux_coordinates[1] << " " << flux_values[0]/sqrt(2) + flux_values[1]/sqrt(2) << "\n";
    //   }
    // }
    flux_dat.close();
    set_log_level(0);
    timer_4.stop();
    set_log_level(30);

    set_log_level(0);
    timer.stop();
    set_log_level(30);

    // Error and Rate calculation
    double h = mesh.hmax();
    double h_ff = mesh_ff.hmax();
    std::cout << "h_max: " << h << std::endl;
    std::cout << "h_max_ff: " << h_ff << std::endl;
    
    if(refidx == refinements)
    {
      File("p_pm_function.xml") << p_pm;
      File("p_ff_function.xml") << p_ff;
    }

    if(refidx < refinements)
    {
      RectangleMesh mesh_fine(-1.05e-4, -0.5e-3, 1.05e-4, 0.5e-3, 2*pow(2, refinements+1), 4*pow(2, refinements+1), "crossed"); 
      Mesh mesh_ff_fine(output_filename("results/mesh_ff", refinements, "_refinement.xml"));

      Error::CoefficientSpace_uold VP_fine(mesh_fine);
      Error_ff::CoefficientSpace_uold VP_ff_fine(mesh_ff_fine);

      Function p_fine(VP_fine, "p_pm_function.xml");
      Function p_ff_fine(VP_ff_fine, "p_ff_function.xml");

      p_pm.interpolate(p_fine);
      p_ff.interpolate(p_ff_fine);

      Error::Functional MP(mesh_fine, p_fine, p_pm);
      Error_ff::Functional MP_ff(mesh_ff_fine, p_ff_fine, p_ff);

      double error = sqrt(assemble(MP));
      double error_ff = sqrt(assemble(MP_ff));

      std::cout << "L2-Norm p_pm: " << error << std::endl;
      std::cout << "L2-Norm p_ff: " << error_ff << std::endl;

      if(refidx < refinements - 1)
      {
        std::cout << "Experimental order of convergence (p_pm): " << (log(error)-log(error_old))/(log(h)-log(h_old)) << std::endl;
        std::cout << "Experimental order of convergence (p_ff): " << (log(error_ff)-log(error_ff_old))/(log(h_ff)-log(h_ff_old)) << std::endl;
      }

      error_old = error;
      error_ff_old = error_ff;
      h_old = h;
      h_ff_old = h_ff;
    }

    
    File(output_filename("results/mesh", refidx, "_refinement.xml")) << mesh;
    File(output_filename("results/mesh_ff", refidx, "_refinement.xml")) << mesh_ff;

  }
  
  return 0;
}
