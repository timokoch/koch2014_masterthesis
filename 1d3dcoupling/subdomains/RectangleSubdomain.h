class Bottom : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], -0.5e-3); //vessel
  }
};

class Top : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], 0.5e-3, 1e-9); //vessel or bifurcation vessel
  }
};

class Right : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[0], 105e-6); //vessel
  }
};

class Left : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[0], -105e-6); //vessel
  }
};

// Sub domain for inflow ff
class Inflow : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], 0.5e-3); //vessel or bifurcation vessel
  }
};

// Sub domain for outflow ff
class Outflow : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && near(x[1], -0.5e-3); //vessel
  }
};

class Vessel : public SubDomain
{
public:

  bool inside(const Array<double>& x, bool on_boundary) const
  {
    
    return near(x[0], 0.0); //vessel
  }
};