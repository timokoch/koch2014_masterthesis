Point(1) = {1e-4, 0.5e-3, 0, 0.125e-3};
Point(2) = {1e-4, 0, 0, 0.125e-3};
Point(3) = {-0.5e-3/Sqrt(2) + 1e-4, -0.5e-3/Sqrt(2), 0, 0.125e-3};
Point(4) = {0.5e-3/Sqrt(2) + 1e-4, -0.5e-3/Sqrt(2), 0, 0.125e-3};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {2, 4};
