cl_1 = 0.25;
cl_2 = 2.0;
Point(1) = {0, 0, 0, cl_1};
Point(2) = {1, 1, 0, cl_1};
Point(3) = {-1, 1, 0, cl_1};
Point(4) = {-1, -1, 0, cl_1};
Point(5) = {1, -1, 0, cl_1};

Circle(1) = {3, 1, 2};
Circle(2) = {2, 1, 5};
Circle(3) = {5, 1, 4};
Circle(4) = {4, 1, 3};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Point(6) = {7, 7, 0, cl_2};
Point(7) = {-7, 7, 0, cl_2};
Point(8) = {7, -7, 0, cl_2};
Point(9) = {-7, -7, 0, cl_2};
Circle(7) = {6, 1, 8};
Circle(8) = {8, 1, 9};
Circle(9) = {9, 1, 7};
Circle(10) = {7, 1, 6};
Line(11) = {6, 2};
Line(12) = {5, 8};
Line(13) = {4, 9};
Line(14) = {3, 7};
Line Loop(15) = {10, 11, -1, 14};
Ruled Surface(16) = {15};
Line Loop(17) = {11, 2, 12, -7};
Ruled Surface(18) = {17};
Line Loop(19) = {3, 13, -8, -12};
Ruled Surface(20) = {19};
Line Loop(21) = {13, 9, -14, -4};
Ruled Surface(22) = {21};
Extrude {0, 0, 1} {
  Surface{18, 20, 22, 16, 6};
}
Translate {0, 0, 1} {
  Duplicata { Volume{5, 4, 1, 2, 3}; }
}
Translate {0, 0, 1} {
  Duplicata { Volume{164, 133, 257, 226, 195}; }
}
Translate {0, 0, 1} {
  Duplicata { Volume{398, 274, 336, 305, 367}; }
}
Translate {0, 0, 1} {
  Duplicata { Volume{549, 425, 518, 456, 487}; }
}
Translate {0, 0, 1} {
  Duplicata { Volume{664, 602, 571, 633, 695}; }
}
Translate {0, 0, 1} {
  Duplicata { Volume{712, 743, 805, 774, 836}; }
}
Translate {0, 0, 1} {
  Duplicata { Volume{853, 884, 915, 977, 946}; }
}
Translate {0, 0, 1} {
  Duplicata { Volume{1087, 994, 1056, 1025, 1118}; }
}
Translate {0, 0, 1} {
  Duplicata { Volume{1264, 1233, 1202, 1140, 1171}; }
}
