cl__1 = 0.25;
Point(1) = {0, 0, 0, 0.25};
Point(2) = {1, 1, 0, 0.25};
Point(3) = {-1, 1, 0, 0.25};
Point(4) = {1, -1, 0, 0.25};
Point(5) = {-1, -1, 0, 0.25};
Point(6) = {-1, -1, 1, 0.25};
Point(7) = {0, 0, 1, 0.25};
Point(8) = {-1, 1, 1, 0.25};
Point(13) = {1, 1, 1, 0.25};
Point(18) = {1, -1, 1, 0.25};
Point(39) = {-1, -1, 2, 0.25};
Point(40) = {0, 0, 2, 0.25};
Point(41) = {-1, 1, 2, 0.25};
Point(46) = {1, 1, 2, 0.25};
Point(51) = {1, -1, 2, 0.25};
Point(72) = {-1, -1, 3, 0.25};
Point(73) = {0, 0, 3, 0.25};
Point(74) = {-1, 1, 3, 0.25};
Point(79) = {1, 1, 3, 0.25};
Point(84) = {1, -1, 3, 0.25};
Point(105) = {-1, -1, 4, 0.25};
Point(106) = {0, 0, 4, 0.25};
Point(107) = {-1, 1, 4, 0.25};
Point(112) = {1, 1, 4, 0.25};
Point(117) = {1, -1, 4, 0.25};
Point(138) = {-1, -1, 5, 0.25};
Point(139) = {0, 0, 5, 0.25};
Point(140) = {-1, 1, 5, 0.25};
Point(145) = {1, 1, 5, 0.25};
Point(150) = {1, -1, 5, 0.25};
Point(171) = {-1, -1, 6, 0.25};
Point(172) = {0, 0, 6, 0.25};
Point(173) = {-1, 1, 6, 0.25};
Point(178) = {1, 1, 6, 0.25};
Point(183) = {1, -1, 6, 0.25};
Point(204) = {-1, -1, 7, 0.25};
Point(205) = {0, 0, 7, 0.25};
Point(206) = {-1, 1, 7, 0.25};
Point(211) = {1, 1, 7, 0.25};
Point(216) = {1, -1, 7, 0.25};
Point(237) = {-1, -1, 8, 0.25};
Point(238) = {0, 0, 8, 0.25};
Point(239) = {-1, 1, 8, 0.25};
Point(244) = {1, 1, 8, 0.25};
Point(249) = {1, -1, 8, 0.25};
Point(270) = {-1, -1, 9, 0.25};
Point(271) = {0, 0, 9, 0.25};
Point(272) = {-1, 1, 9, 0.25};
Point(277) = {1, 1, 9, 0.25};
Point(282) = {1, -1, 9, 0.25};
Point(303) = {-1, -1, 10, 0.25};
Point(304) = {0, 0, 10, 0.25};
Point(305) = {-1, 1, 10, 0.25};
Point(310) = {1, 1, 10, 0.25};
Point(315) = {1, -1, 10, 0.25};
Circle(1) = {3, 1, 2};
Circle(2) = {2, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 3};
Circle(8) = {6, 7, 8};
Circle(9) = {8, 7, 13};
Circle(10) = {13, 7, 18};
Circle(11) = {18, 7, 6};
Line(13) = {5, 6};
Line(14) = {3, 8};
Line(18) = {2, 13};
Line(22) = {4, 18};
Circle(38) = {39, 40, 41};
Circle(39) = {41, 40, 46};
Circle(40) = {46, 40, 51};
Circle(41) = {51, 40, 39};
Line(44) = {8, 41};
Line(46) = {39, 6};
Line(49) = {13, 46};
Line(54) = {18, 51};
Circle(65) = {72, 73, 74};
Circle(66) = {74, 73, 79};
Circle(67) = {79, 73, 84};
Circle(68) = {84, 73, 72};
Line(71) = {41, 74};
Line(73) = {72, 39};
Line(76) = {46, 79};
Line(81) = {51, 84};
Circle(92) = {105, 106, 107};
Circle(93) = {107, 106, 112};
Circle(94) = {112, 106, 117};
Circle(95) = {117, 106, 105};
Line(98) = {74, 107};
Line(100) = {105, 72};
Line(103) = {79, 112};
Line(108) = {84, 117};
Circle(119) = {138, 139, 140};
Circle(120) = {140, 139, 145};
Circle(121) = {145, 139, 150};
Circle(122) = {150, 139, 138};
Line(125) = {107, 140};
Line(127) = {138, 105};
Line(130) = {112, 145};
Line(135) = {117, 150};
Circle(146) = {171, 172, 173};
Circle(147) = {173, 172, 178};
Circle(148) = {178, 172, 183};
Circle(149) = {183, 172, 171};
Line(152) = {140, 173};
Line(154) = {171, 138};
Line(157) = {145, 178};
Line(162) = {150, 183};
Circle(173) = {204, 205, 206};
Circle(174) = {206, 205, 211};
Circle(175) = {211, 205, 216};
Circle(176) = {216, 205, 204};
Line(179) = {173, 206};
Line(181) = {204, 171};
Line(184) = {178, 211};
Line(189) = {183, 216};
Circle(200) = {237, 238, 239};
Circle(201) = {239, 238, 244};
Circle(202) = {244, 238, 249};
Circle(203) = {249, 238, 237};
Line(206) = {206, 239};
Line(208) = {237, 204};
Line(211) = {211, 244};
Line(216) = {216, 249};
Circle(227) = {270, 271, 272};
Circle(228) = {272, 271, 277};
Circle(229) = {277, 271, 282};
Circle(230) = {282, 271, 270};
Line(233) = {239, 272};
Line(235) = {270, 237};
Line(238) = {244, 277};
Line(243) = {249, 282};
Circle(254) = {303, 304, 305};
Circle(255) = {305, 304, 310};
Circle(256) = {310, 304, 315};
Circle(257) = {315, 304, 303};
Line(260) = {272, 305};
Line(262) = {303, 270};
Line(265) = {277, 310};
Line(270) = {282, 315};
Line Loop(6) = {4, 1, 2, 3};
Plane Surface(6) = {6};
Line Loop(15) = {4, 14, -8, -13};
Ruled Surface(15) = {15};
Line Loop(19) = {1, 18, -9, -14};
Ruled Surface(19) = {19};
Line Loop(23) = {2, 22, -10, -18};
Ruled Surface(23) = {23};
Line Loop(27) = {3, 13, -11, -22};
Ruled Surface(27) = {27};
Line Loop(28) = {8, 9, 10, 11};
Plane Surface(28) = {28};
Line Loop(37) = {38, 39, 40, 41};
Plane Surface(37) = {37};
Line Loop(42) = {8, 44, -38, 46};
Ruled Surface(42) = {42};
Line Loop(47) = {9, 49, -39, -44};
Ruled Surface(47) = {47};
Line Loop(52) = {10, 54, -40, -49};
Ruled Surface(52) = {52};
Line Loop(57) = {11, -46, -41, -54};
Ruled Surface(57) = {57};
Line Loop(64) = {65, 66, 67, 68};
Plane Surface(64) = {64};
Line Loop(69) = {38, 71, -65, 73};
Ruled Surface(69) = {69};
Line Loop(74) = {39, 76, -66, -71};
Ruled Surface(74) = {74};
Line Loop(79) = {40, 81, -67, -76};
Ruled Surface(79) = {79};
Line Loop(84) = {41, -73, -68, -81};
Ruled Surface(84) = {84};
Line Loop(91) = {92, 93, 94, 95};
Plane Surface(91) = {91};
Line Loop(96) = {65, 98, -92, 100};
Ruled Surface(96) = {96};
Line Loop(101) = {66, 103, -93, -98};
Ruled Surface(101) = {101};
Line Loop(106) = {67, 108, -94, -103};
Ruled Surface(106) = {106};
Line Loop(111) = {68, -100, -95, -108};
Ruled Surface(111) = {111};
Line Loop(118) = {119, 120, 121, 122};
Plane Surface(118) = {118};
Line Loop(123) = {92, 125, -119, 127};
Ruled Surface(123) = {123};
Line Loop(128) = {93, 130, -120, -125};
Ruled Surface(128) = {128};
Line Loop(133) = {94, 135, -121, -130};
Ruled Surface(133) = {133};
Line Loop(138) = {95, -127, -122, -135};
Ruled Surface(138) = {138};
Line Loop(145) = {146, 147, 148, 149};
Plane Surface(145) = {145};
Line Loop(150) = {119, 152, -146, 154};
Ruled Surface(150) = {150};
Line Loop(155) = {120, 157, -147, -152};
Ruled Surface(155) = {155};
Line Loop(160) = {121, 162, -148, -157};
Ruled Surface(160) = {160};
Line Loop(165) = {122, -154, -149, -162};
Ruled Surface(165) = {165};
Line Loop(172) = {173, 174, 175, 176};
Plane Surface(172) = {172};
Line Loop(177) = {146, 179, -173, 181};
Ruled Surface(177) = {177};
Line Loop(182) = {147, 184, -174, -179};
Ruled Surface(182) = {182};
Line Loop(187) = {148, 189, -175, -184};
Ruled Surface(187) = {187};
Line Loop(192) = {149, -181, -176, -189};
Ruled Surface(192) = {192};
Line Loop(199) = {200, 201, 202, 203};
Plane Surface(199) = {199};
Line Loop(204) = {173, 206, -200, 208};
Ruled Surface(204) = {204};
Line Loop(209) = {174, 211, -201, -206};
Ruled Surface(209) = {209};
Line Loop(214) = {175, 216, -202, -211};
Ruled Surface(214) = {214};
Line Loop(219) = {176, -208, -203, -216};
Ruled Surface(219) = {219};
Line Loop(226) = {227, 228, 229, 230};
Plane Surface(226) = {226};
Line Loop(231) = {200, 233, -227, 235};
Ruled Surface(231) = {231};
Line Loop(236) = {201, 238, -228, -233};
Ruled Surface(236) = {236};
Line Loop(241) = {202, 243, -229, -238};
Ruled Surface(241) = {241};
Line Loop(246) = {203, -235, -230, -243};
Ruled Surface(246) = {246};
Line Loop(253) = {254, 255, 256, 257};
Plane Surface(253) = {253};
Line Loop(258) = {227, 260, -254, 262};
Ruled Surface(258) = {258};
Line Loop(263) = {228, 265, -255, -260};
Ruled Surface(263) = {263};
Line Loop(268) = {229, 270, -256, -265};
Ruled Surface(268) = {268};
Line Loop(273) = {230, -262, -257, -270};
Ruled Surface(273) = {273};
Surface Loop(1) = {6, 28, 15, 19, 23, 27};
Volume(1) = {1};
Surface Loop(30) = {28, 15, 6, 19, 23, 27};
Volume(30) = {30};
Surface Loop(31) = {28, 37, 42, 47, 52, 57};
Volume(31) = {31};
Surface Loop(58) = {37, 64, 69, 74, 79, 84};
Volume(58) = {58};
Surface Loop(85) = {64, 91, 96, 101, 106, 111};
Volume(85) = {85};
Surface Loop(112) = {91, 118, 123, 128, 133, 138};
Volume(112) = {112};
Surface Loop(139) = {118, 145, 150, 155, 160, 165};
Volume(139) = {139};
Surface Loop(166) = {145, 172, 177, 182, 187, 192};
Volume(166) = {166};
Surface Loop(193) = {172, 199, 204, 209, 214, 219};
Volume(193) = {193};
Surface Loop(220) = {199, 226, 231, 236, 241, 246};
Volume(220) = {220};
Surface Loop(247) = {226, 253, 258, 263, 268, 273};
Volume(247) = {247};
Point(316) = {10, -10, 0, 0.25};
Point(317) = {-10, 10, 0, 0.25};
Point(318) = {10, 10, 0, 0.25};
Point(319) = {-10, -10, 0, 0.25};
Circle(258) = {316, 1, 319};
Circle(259) = {319, 1, 317};
Circle(261) = {318, 1, 316};
