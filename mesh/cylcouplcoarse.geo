cl_1 = 0.5;
cl_2 = 4.0;
Point(1) = {0, 0, 0, cl_1};
Point(2) = {1, 1, 0, cl_1};
Point(3) = {-1, 1, 0, cl_1};
Point(4) = {-1, -1, 0, cl_1};
Point(5) = {1, -1, 0, cl_1};

Circle(1) = {3, 1, 2};
Circle(2) = {2, 1, 5};
Circle(3) = {5, 1, 4};
Circle(4) = {4, 1, 3};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Point(6) = {7, 7, 0, cl_2};
Point(7) = {-7, 7, 0, cl_2};
Point(8) = {7, -7, 0, cl_2};
Point(9) = {-7, -7, 0, cl_2};
Circle(7) = {6, 1, 8};
Circle(8) = {8, 1, 9};
Circle(9) = {9, 1, 7};
Circle(10) = {7, 1, 6};
Line(11) = {6, 2};
Line(12) = {5, 8};
Line(13) = {4, 9};
Line(14) = {3, 7};
Line Loop(15) = {10, 11, -1, 14};
Ruled Surface(16) = {15};
Line Loop(17) = {11, 2, 12, -7};
Ruled Surface(18) = {17};
Line Loop(19) = {3, 13, -8, -12};
Ruled Surface(20) = {19};
Line Loop(21) = {13, 9, -14, -4};
Ruled Surface(22) = {21};
Extrude {0, 0, 2} {
  Surface{18, 20, 22, 16, 6};
}
Translate {0, 0, 2} {
  Duplicata { Volume{1, 4, 5, 3, 2}; }
}
Translate {0, 0, 4} {
  Duplicata { Volume{1, 4, 5, 3, 2}; }
}
Translate {0, 0, 6} {
  Duplicata { Volume{1, 4, 5, 3, 2}; }
}
Translate {0, 0, 8} {
  Duplicata { Volume{1, 4, 5, 3, 2}; }
}
